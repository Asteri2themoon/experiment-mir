import torch
import torch.nn as nn
import git

from settings import Settings, HParams
from modules import Activation, PixelShuffle2D, View, VariationalBayes

import math
import warnings
from typing import Type, Any
import importlib


class GitRepoNotFoundWarning(UserWarning):
    pass


class PerformanceWarning(UserWarning):
    pass


class ClassNameDontMatchWarning(UserWarning):
    pass


class CommitDontMatchWarning(UserWarning):
    pass


class DeepModel(nn.Module):
    __scope__ = "model.deepmodel"

    try:
        repo = git.Repo(search_parent_directories=True)
    except git.exc.InvalidGitRepositoryError:
        __commit__ = None

        warnings.warn(
            "Warning: git Repo not found, can't log commit number",
            GitRepoNotFoundWarning,
        )
    else:
        __commit__ = repo.head.object.hexsha

    def __init__(self, **kwargs):
        super(DeepModel, self).__init__()

        self.hparams = Settings()
        with self.scope() as p:
            p.merge(HParams(**kwargs))

    def to_dict(self):
        return {
            "module": self.__module__,
            "class": self.__class__.__name__,
            "commit": self.__commit__,
            "hparams": self.hparams.to_dict(),
            "parameters": self.cpu().state_dict(),
        }

    def from_dict(self, dct: dict, module_name: str = None):
        if dct["commit"] != DeepModel.__commit__:

            warnings.warn(
                f"Warning: commit {dct['commit']} of this dct don't match with the current commit {DeepModel.__commit__}",
                CommitDontMatchWarning,
            )

        if module_name is None:
            module_name = dct["module"]

        module = importlib.import_module(module_name)
        class_ = getattr(module, dct["class"])
        settings = Settings()
        settings.from_dict(dct["hparams"])

        with settings.scope(class_.__scope__) as p:
            model = class_(**p.params)
            self.__class__ = model.__class__
            self.__dict__ = model.__dict__

        self.load_state_dict(dct["parameters"])
        self.eval()

    def save(self, file_name):
        torch.save(
            self.to_dict(), file_name,
        )

    @staticmethod
    def load(file_name: str, module_name: str = None) -> Any:
        checkpoint = torch.load(file_name, map_location="cpu")

        model = DeepModel()
        model.from_dict(checkpoint, module_name)

        return model

    @staticmethod
    def info(file_name, verbose=True):
        checkpoint = torch.load(file_name, map_location="cpu")
        print("module name:", checkpoint["module"])
        print("class name:", checkpoint["class"])
        print("project commit:", checkpoint["commit"])
        print("hyperparameters:", checkpoint["hparams"])
        return checkpoint

    def scope(self):
        return self.hparams.scope(self.__scope__)


class FcBlock(DeepModel):
    __scope__ = "model.fcblock"

    with Settings.default.scope(__scope__) as hparams:
        hparams.in_features = 0
        hparams.out_features = 0
        hparams.bias = True
        hparams.dropout = 0.25
        hparams.batch_norm = True
        hparams.activation = "relu"

    def __init__(self, **kwargs):
        super(FcBlock, self).__init__(**kwargs)

        with self.scope() as p:
            assert p.in_features > 0

            # setup default value
            if p.out_features == 0:
                p.out_features = p.in_features

            layers = []
            # build standard fully-connected block
            layers.append(nn.Linear(p.in_features, p.out_features, bias=p.bias))
            if p.batch_norm:
                layers.append(nn.BatchNorm1d(p.out_features))
            if p.activation != "linear":
                layers.append(Activation(p.activation))
            if p.dropout > 0:
                layers.append(nn.Dropout(p.dropout))

            # return sequential network
            self.fc = nn.Sequential(*layers)

    def output_shape(self, input_shape: tuple) -> tuple:
        assert type(input_shape) == tuple and type(input_shape[0]) == int

        (f_in,) = input_shape
        with self.scope() as p:
            if f_in != p.in_features:

                class InputShapeDontMatchException(Exception):
                    pass

                raise InputShapeDontMatchException(
                    f"Input shape don't match ({f_in} != {p.in_features})"
                )

            f_out = p.out_features

        if type(f_out) == torch.Tensor:
            f_out = int(f_out.item())
        return (f_out,)

    def forward(self, x):
        return self.fc(x)


class ConvBlock(DeepModel):
    __scope__ = "model.convblock"

    with Settings.default.scope(__scope__) as hparams:
        hparams.in_channels = 0
        hparams.out_channels = 0
        hparams.kernel_size = (3, 3)
        hparams.dropout = 0.25
        hparams.batch_norm = True
        hparams.scale = (1, 1)
        hparams.type = "conv"
        hparams.activation = "relu"

    def __init__(self, **kwargs):
        super(ConvBlock, self).__init__(**kwargs)

        with self.scope() as p:
            assert p.in_channels > 0

            # setup default value
            if p.out_channels == 0:
                p.out_channels = p.in_channels

            self.__scale__ = p.scale
            self.__type__ = p.type

            layers = []
            # build standard convolution block
            if p.type == "conv":
                self.padding = [dim_size // 2 for dim_size in p.kernel_size]
                self.stride = p.scale
                layers.append(
                    nn.Conv2d(
                        p.in_channels,
                        p.out_channels,
                        p.kernel_size,
                        self.stride,
                        padding=self.padding,
                    )
                )
            elif p.type == "deconv":
                # check size
                for s, k in zip(p.scale, p.kernel_size):
                    if s == 1 and (k % 2) == 0:
                        raise Exception("can't use even kernel size with stride 1")

                # setup
                self.stride = p.scale
                self.padding = [dim_size // 2 for dim_size in p.kernel_size]
                self.output_padding = [
                    s + 2 * p - k
                    for s, p, k in zip(self.stride, self.padding, p.kernel_size)
                ]
                if self.padding[0] == 1 and p.scale[0] == 1 and p.kernel_size[0] == 2:
                    self.output_padding = (0, 0)

                # add layer
                layers.append(
                    nn.ConvTranspose2d(
                        p.in_channels,
                        p.out_channels,
                        kernel_size=p.kernel_size,
                        stride=self.stride,
                        padding=self.padding,
                        output_padding=self.output_padding,
                    )
                )
            elif p.type == "upsample":
                layers.append(
                    nn.Upsample(
                        scale_factor=p.scale, mode="bilinear", align_corners=False
                    )
                )
                self.padding = [dim_size // 2 for dim_size in p.kernel_size]
                layers.append(
                    nn.Conv2d(
                        p.in_channels,
                        p.out_channels,
                        p.kernel_size,
                        padding=self.padding,
                    )
                )
            elif p.type == "pixelshuffle":
                self.padding = [dim_size // 2 for dim_size in p.kernel_size]
                layers.append(
                    nn.Conv2d(
                        p.in_channels,
                        p.out_channels * p.scale[0] * p.scale[1],
                        p.kernel_size,
                        padding=self.padding,
                    )
                )
                layers.append(PixelShuffle2D(p.scale))
            else:
                raise Exception(f"ConvBlock type {p.type} don't existe")
            # regularisation and activation function
            if p.batch_norm:
                layers.append(nn.BatchNorm2d(p.out_channels))
            if p.activation != "linear":
                layers.append(Activation(p.activation))
            if p.dropout > 0:
                layers.append(nn.Dropout2d(p.dropout))

            # return sequential network
            self.conv = nn.Sequential(*layers)

    def output_shape(self, input_shape: Any) -> Type[torch.Tensor]:
        assert type(input_shape) == tuple and len(input_shape) == 3

        c_in, h_in, w_in = input_shape
        if type(c_in) == torch.Tensor:
            c_in = int(c_in.item())
        if type(h_in) == torch.Tensor:
            h_in = int(h_in.item())
        if type(w_in) == torch.Tensor:
            w_in = int(w_in.item())

        with self.scope() as p:
            if c_in != p.in_channels:

                class InputShapeDontMatchException(Exception):
                    pass

                raise InputShapeDontMatchException(
                    f"Input shape don't match ({c_in} != {p.in_channels})"
                )

            if p.type in ["conv"]:
                h_out = h_in // p.scale[0]
                w_out = w_in // p.scale[1]
                if h_out < 1:
                    h_out = 1
                if w_out < 1:
                    w_out = 1
            elif p.type in ["deconv", "upsample", "pixelshuffle"]:
                h_out = h_in * p.scale[0]
                w_out = w_in * p.scale[1]
            else:
                raise Exception(f"ConvBlock type {p.type} don't existe")
            c_out = p.out_channels
        return (c_out, h_out, w_out)

    def forward(self, x):
        h_in, w_in = x.shape[2:]

        if self.__type__ in ["conv"]:
            h_out = h_in // self.__scale__[0]
            w_out = w_in // self.__scale__[1]
        elif self.__type__ in ["deconv", "upsample", "pixelshuffle"]:
            h_out = h_in * self.__scale__[0]
            w_out = w_in * self.__scale__[1]
        else:
            raise Exception(f"ConvBlock type {self.type} don't existe")

        y = self.conv(x)

        if y.shape[2:] != (h_out, w_out):
            warnings.warn(
                f"Warning: Performance issue, use this output shape with kernel size implies discontiguous memory",
                PerformanceWarning,
            )
            y = y[:, :, :h_out, :w_out].contiguous()

        return y
