import unittest
import os
from collections import namedtuple

import train_specvae


class TestTraining(unittest.TestCase):
    base_dir = "unittest/train_specvae"
    try:
        os.makedirs(base_dir)
    except OSError:
        pass

    def test_training(self):
        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers from_checkpoint to_checkpoint device hparams logs",
        )
        args = Arguments(
            os.path.join(self.base_dir, "maestro-preprocess"),
            2,
            None,
            os.path.join(self.base_dir, "checkpoint.pth"),
            "cpu",
            os.path.join(self.base_dir, "config.json"),
            os.path.join(self.base_dir, "runs/SpecVAE"),
        )

        train_specvae.main(args)


if __name__ == "__main__":
    unittest.main()
