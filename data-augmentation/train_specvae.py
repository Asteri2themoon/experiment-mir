import torch
import torch.optim as optim
import torch.nn as nn
import pandas as pd

from train import Training
from settings import Settings
from specvae import SpecVAE
from dataset import get_dataloader

import os


class TrainSpecVae(Training):
    __scope__ = "training"
    with Settings.default.scope(__scope__) as hparams:
        hparams.batch_size = 128
        hparams.epoch = 16
        hparams.loss.kld_threshold = 0.01
        hparams.lr = 1e-4
        hparams.spec_type = "mel"
        hparams.dataset_workers = 1

    def __init__(self, **kwargs):
        super(TrainSpecVae, self).__init__(**kwargs)

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        state_optim: dict = None,
        state_model: dict = None,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            self.epoch = p.epoch
            self.kld_threshold = torch.tensor(
                p.loss.kld_threshold, dtype=torch.float32
            ).to(self.device)

        self.model = SpecVAE(**hparams.params)

        if state_model is not None:
            self.model.from_dict(state_model)

        with hparams.scope(self.__scope__) as p:
            self.optimizer = optim.Adam(self.model.parameters(), lr=p.lr)
            self.loss = nn.MSELoss()

        if state_optim is not None:
            self.optimizer.load_state_dict(state_optim)

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            if train:
                path_train = os.path.join(dataset_path, "train")
                self.train_loader = get_dataloader(
                    path_train,
                    batch_size=self.batch_size,
                    spec_type=p.spec_type,
                    num_workers=p.dataset_workers,
                )
            if validation:
                path_validation = os.path.join(dataset_path, "validation")
                self.validation_loader = get_dataloader(
                    path_validation,
                    batch_size=self.batch_size,
                    spec_type=p.spec_type,
                    num_workers=p.dataset_workers,
                )
            if test:
                path_test = os.path.join(dataset_path, "test")
                self.test_loader = get_dataloader(
                    path_test,
                    batch_size=self.batch_size,
                    spec_type=p.spec_type,
                    num_workers=p.dataset_workers,
                )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer.zero_grad()

        x = data[0][0]
        x_tilde = model(x)

        l_kld = model.KL_divergence
        l_recon = loss(x_tilde, x)
        l = l_recon + torch.max(l_kld, self.kld_threshold)

        l.backward()
        optimizer.step()

        return {"loss": l, "loss_kld": l_kld, "loss_recon": l_recon}

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        x = data[0][0]
        x_tilde = model(x)

        l_kld = model.KL_divergence
        l_recon = loss(x_tilde, x)
        l = l_recon + l_kld

        return {"loss": l, "loss_kld": l_kld, "loss_recon": l_recon}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        x = data[0][0]
        x_tilde = model(x)

        l_kld = model.KL_divergence
        l_recon = loss(x_tilde, x)
        l = l_recon + l_kld

        return {"loss": l, "loss_kld": l_kld, "loss_recon": l_recon}

    # calculate metrics
    def summarise_metrics(self, metrics: pd.DataFrame) -> dict:
        results = {}

        if "loss" in metrics:
            results["loss"] = metrics["loss"].mean()
            results["loss_kld"] = metrics["loss_kld"].mean()
            results["loss_recon"] = metrics["loss_recon"].mean()

        return results

    # display metrics
    def display_metrics(self, metrics: dict) -> str:
        results = ""

        if "loss" in metrics:
            results += f"loss: {metrics['loss']:.3f} "
        if "loss_kld" in metrics:
            results += f"kld: {metrics['loss_kld']:.3f} "
        if "loss_recon" in metrics:
            results += f"recon: {metrics['loss_recon']:.3f} "

        return results


def main(args):
    hparams = Settings()
    hparams.load(args.hparams)
    hparams.dataset_workers = args.dataset_workers

    training = TrainSpecVae(
        dataset_path=args.dataset_path,
        from_checkpoint=args.from_checkpoint,
        to_checkpoint=args.to_checkpoint,
        device=args.device,
        hparams=hparams,
        logs=args.logs,
    )
    training.run()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="train SpecVAE")

    parser.add_argument("--dataset-path", "-dp", help="path to the dataset directory")
    parser.add_argument(
        "--dataset-workers",
        "-dw",
        type=int,
        default=8,
        help="path to the dataset directory",
    )
    parser.add_argument(
        "--dataset-demo",
        "-dd",
        default=None,
        help="path to a raw maestro dataset to save demo and the end of the training into the tensorflow",
    )
    parser.add_argument(
        "--from-checkpoint",
        "-fc",
        default=None,
        help="Start training from a checkpoint",
    )
    parser.add_argument(
        "--to-checkpoint", "-tc", default=None, help="output checkpoint",
    )
    parser.add_argument(
        "--device", "-d", default="cpu", help="device to run the training"
    )
    parser.add_argument(
        "--hparams",
        "-hp",
        default=None,
        help="hyperparameters configuration, path to the file or json format",
    )
    parser.add_argument("--logs", "-l", default="runs", help="logs directory")

    args = parser.parse_args()

    main(args)
