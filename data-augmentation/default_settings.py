import importlib
import os

from settings import Settings
import specvae
import train_specvae


def main(args):
    default = Settings()
    default.save(args.hparams)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="generate default settings")

    parser.add_argument(
        "--hparams",
        "-hp",
        default="config.json",
        help="hyperparameters configuration, path to the file or json format",
    )

    args = parser.parse_args()

    main(args)
