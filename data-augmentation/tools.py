import sys
import os
import signal
import time


class Killer:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit)
        signal.signal(signal.SIGTERM, self.exit)

    def exit(self, signum, frame):
        print("kill signal received, stopping")
        sys.stdout.flush()
        self.kill_now = True


def get_n_params(model):
    pp = 0
    for p in list(model.parameters()):
        nn = 1
        for s in list(p.size()):
            nn = nn * s
        pp += nn
    return pp


# setup output directory
def create_dir(base, dir):
    try:
        path = os.path.join(base, dir)
        os.makedirs(path)
    except FileExistsError:
        pass
    return path


def duration_display(duration):
    hours, rem = divmod(duration, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:0>6.3f}".format(int(hours), int(minutes), seconds)


def timer(start, end):
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:0>6.3f}".format(int(hours), int(minutes), seconds)


def remaining_time(start: float, current: float, progress: float):
    assert type(start) == float and start > 0
    assert type(current) == float and current > 0
    assert start < current
    assert type(progress) == float and 0 <= progress <= 1

    remaining = (current - start) * (1 - progress) / progress

    hours, rem = divmod(remaining, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:0>4.1f}".format(int(hours), int(minutes), seconds)
