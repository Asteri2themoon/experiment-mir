import torch
import torch.nn as nn

from modules import Activation, PixelShuffle2D, View, VariationalBayes
from models import DeepModel, ConvBlock, FcBlock
from specvae import SpecVAE
from settings import Settings, HParams

import warnings
import unittest
import random
import os


class Test(DeepModel):
    __scope__ = "model.test"

    with Settings.default.scope(__scope__) as hparams:
        hparams.value = -1.0

    def __init__(self, **kwargs):
        super(Test, self).__init__(**kwargs)
        with self.scope() as p:
            self.value = nn.Parameter(torch.tensor(p.value), requires_grad=True)

    def forward(self, x):
        return x + self.value


class TestModels(unittest.TestCase):
    base_dir = "unittest/models"
    try:
        os.makedirs(base_dir)
    except OSError:
        pass

    def test_activation(self):
        assert type(Activation("relu")) == nn.ReLU
        assert type(Activation("sigmoid")) == nn.Sigmoid
        assert type(Activation("tanh")) == nn.Tanh

    def test_pixelshuffle2d(self):
        test1 = PixelShuffle2D(upsample=(2, 3))
        x1 = torch.tensor(
            [
                [
                    [[1, 1], [1, 1]],
                    [[2, 2], [2, 2]],
                    [[3, 3], [3, 3]],
                    [[4, 4], [4, 4]],
                    [[5, 5], [5, 5]],
                    [[6, 6], [6, 6]],
                    [[7, 7], [7, 7]],
                    [[8, 8], [8, 8]],
                    [[9, 9], [9, 9]],
                    [[10, 10], [10, 10]],
                    [[11, 11], [11, 11]],
                    [[12, 12], [12, 12]],
                ]
            ],
            dtype=float,
        )
        y1 = torch.tensor(
            [
                [
                    [
                        [1, 2, 3, 1, 2, 3],
                        [4, 5, 6, 4, 5, 6],
                        [1, 2, 3, 1, 2, 3],
                        [4, 5, 6, 4, 5, 6],
                    ],
                    [
                        [7, 8, 9, 7, 8, 9],
                        [10, 11, 12, 10, 11, 12],
                        [7, 8, 9, 7, 8, 9],
                        [10, 11, 12, 10, 11, 12],
                    ],
                ]
            ],
            dtype=float,
        )
        y1p = test1(x1)
        assert y1.shape == y1p.shape
        assert torch.norm(y1 - y1p) == 0

    def test_view(self):
        def testView(shape):
            n = torch.prod(torch.tensor(shape)).item()
            x = torch.randn((4, n))
            view = View(shape)
            y = view(x)
            assert y.shape == ((4,) + shape)

        testView((1, 2, 3, 4))
        testView((1,))
        testView((20,))
        testView((10, 3))
        testView((1, 4, 4))
        testView((3, 4, 4))

    def test_variationalbayes(self):
        test1 = VariationalBayes(32, 32)
        x1 = torch.randn((4, 32))
        z1 = test1.encode(x1)
        x_tilde1 = test1(x1)

        assert x1.shape == (4, 32)
        assert z1.shape == (4, 32)
        assert x_tilde1.shape == (4, 32)
        assert test1.decode(z1).shape == (4, 32)
        assert len(test1.KL_divergence.shape) == 0

        test2 = VariationalBayes(16, 32)
        x2 = torch.randn((4, 32))
        z2 = test2.encode(x2)
        x_tilde2 = test2(x2)

        assert x2.shape == (4, 32)
        assert z2.shape == (4, 16)
        assert x_tilde2.shape == (4, 32)
        assert test2.decode(z2).shape == (4, 32)
        assert len(test2.KL_divergence.shape) == 0

    def test_deepmodel(self):
        file_name = os.path.join(self.base_dir, "test.pth")

        rand_n = random.random()
        test1 = Test(value=rand_n)
        x = torch.rand(1)
        y1 = test1(x)

        test1.save(file_name)

        test2 = DeepModel.load(file_name)
        y2 = test2(x)

        assert y1 == y2

    def test_fcblock(self):
        def test(batch_size, **kwargs):
            test_fc = FcBlock(**kwargs)
            if not ("out_features" in kwargs):
                kwargs["out_features"] = kwargs["in_features"]

            output_shape = test_fc.output_shape((kwargs["in_features"],))

            x = torch.randn((batch_size, kwargs["in_features"]))
            y = test_fc(x)

            assert y.shape == (batch_size, kwargs["out_features"])
            assert output_shape == (kwargs["out_features"],)

        test(4, in_features=16)
        test(4, in_features=16, out_features=32)
        test(4, in_features=16, bias=False)
        test(4, in_features=16, batch_norm=False)
        test(4, in_features=16, activation="linear")
        test(4, in_features=16, activation="relu")
        test(4, in_features=16, activation="sigmoid")
        test(4, in_features=16, activation="tanh")
        test(4, in_features=16, dropout=0)
        test(4, in_features=16, dropout=0.5)
        test(4, in_features=16, dropout=1.0)

    def test_convblock(self):
        def test_shape(batch_size, input_shape, output_shape, **kwargs):
            test_conv = ConvBlock(
                in_channels=input_shape[0], out_channels=output_shape[0], **kwargs
            )

            test_output_shape = test_conv.output_shape(input_shape)

            x = torch.randn((batch_size,) + input_shape)
            y = test_conv(x)

            assert y.shape == (batch_size,) + output_shape
            assert output_shape == test_output_shape

        def test_kernel_size(batch_size, input_shape, output_shape, **kwargs):
            test_shape(batch_size, input_shape, output_shape, **kwargs)
            test_shape(
                batch_size, input_shape, output_shape, kernel_size=(3, 3), **kwargs
            )
            test_shape(
                batch_size, input_shape, output_shape, kernel_size=(1, 3), **kwargs
            )
            test_shape(
                batch_size, input_shape, output_shape, kernel_size=(3, 1), **kwargs
            )
            test_shape(batch_size, input_shape, output_shape, kernel_size=3, **kwargs)
            test_shape(batch_size, input_shape, output_shape, kernel_size=5, **kwargs)
            test_shape(batch_size, input_shape, output_shape, kernel_size=7, **kwargs)
            if "type" in kwargs and kwargs["type"] != "deconv":
                warnings.filterwarnings("ignore")
                # raise warning is normal there, desired behaviour
                test_shape(
                    batch_size, input_shape, output_shape, kernel_size=2, **kwargs
                )
                test_shape(
                    batch_size, input_shape, output_shape, kernel_size=4, **kwargs
                )
                warnings.filterwarnings("default")

        test_shape(4, (4, 8, 8), (4, 8, 8), dropout=0)
        test_shape(4, (4, 8, 8), (4, 8, 8), dropout=0.5)
        test_shape(4, (4, 8, 8), (4, 8, 8), dropout=1)
        test_shape(4, (4, 8, 8), (4, 8, 8), batch_norm=True)
        test_shape(4, (4, 8, 8), (4, 8, 8), batch_norm=False)
        test_shape(4, (4, 8, 8), (4, 8, 8), activation="linear")
        test_shape(4, (4, 8, 8), (4, 8, 8), activation="relu")
        test_shape(4, (4, 8, 8), (4, 8, 8), activation="sigmoid")
        test_shape(4, (4, 8, 8), (4, 8, 8), activation="tanh")

        test_kernel_size(4, (4, 8, 8), (4, 8, 8))
        test_kernel_size(4, (4, 8, 8), (4, 4, 4), type="conv", scale=(2, 2))
        test_kernel_size(4, (4, 8, 8), (4, 8, 4), type="conv", scale=(1, 2))
        test_kernel_size(4, (4, 8, 8), (4, 4, 8), type="conv", scale=(2, 1))
        test_kernel_size(4, (4, 8, 8), (4, 4, 4), type="conv", scale=2)
        test_kernel_size(4, (4, 9, 9), (4, 3, 3), type="conv", scale=3)

        test_kernel_size(4, (4, 4, 4), (4, 4, 4), type="deconv")
        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="deconv", scale=(2, 2))
        test_kernel_size(4, (4, 4, 4), (4, 4, 8), type="deconv", scale=(1, 2))
        test_kernel_size(4, (4, 4, 4), (4, 8, 4), type="deconv", scale=(2, 1))
        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="deconv", scale=2)
        test_kernel_size(4, (4, 4, 4), (4, 12, 12), type="deconv", scale=3)

        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="upsample", scale=(2, 2))
        test_kernel_size(4, (4, 4, 4), (4, 4, 8), type="upsample", scale=(1, 2))
        test_kernel_size(4, (4, 4, 4), (4, 8, 4), type="upsample", scale=(2, 1))
        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="upsample", scale=2)
        test_kernel_size(4, (4, 4, 4), (4, 12, 12), type="upsample", scale=3)

        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="pixelshuffle", scale=(2, 2))
        test_kernel_size(4, (4, 4, 4), (4, 4, 8), type="pixelshuffle", scale=(1, 2))
        test_kernel_size(4, (4, 4, 4), (4, 8, 4), type="pixelshuffle", scale=(2, 1))
        test_kernel_size(4, (4, 4, 4), (4, 8, 8), type="pixelshuffle", scale=2)
        test_kernel_size(4, (4, 4, 4), (4, 12, 12), type="pixelshuffle", scale=3)

    def test_specvae(self):
        def test(input_shape, latent_dim=64, **kwargs):
            model = SpecVAE(**kwargs)
            x = torch.rand(input_shape)

            x_tilde = model(x)

            z = model.encode(x)
            x_tilde2 = model.decode(z)

            assert x_tilde.shape == input_shape
            assert x_tilde2.shape == input_shape
            assert z.shape == (input_shape[0], latent_dim)
            assert len(model.KL_divergence.shape) == 0

        test(
            (4, 1, 5, 256),
            latent_dim=128,
            vae=HParams(latent_dim=128, beta=10.0),
            dense=HParams(batch_norm=True, activation="relu", dropout=0.0),
        )
        test(
            (4, 1, 5, 256), decoder=HParams(type="deconv"),
        )
        test(
            (4, 1, 5, 256), decoder=HParams(type="upsample"),
        )
        test(
            (4, 1, 5, 256), decoder=HParams(type="pixelshuffle"),
        )
        test(
            (4, 1, 5, 256), conv=HParams(),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(
                conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),
                conv2=HParams(out_channels=128, dropout=0.25, scale=(1, 2)),
                conv4=HParams(out_channels=512, dropout=0.25, scale=(1, 2)),
                conv3=HParams(out_channels=256, dropout=0.25, scale=(1, 2)),
            ),
        )
        test(
            (4, 1, 5, 256), conv=HParams(), dense=HParams(layers=0),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),),
            dense=HParams(layers=0),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(
                conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),
                conv2=HParams(out_channels=128, dropout=0.25, scale=(1, 2)),
                conv4=HParams(out_channels=512, dropout=0.25, scale=(1, 2)),
                conv3=HParams(out_channels=256, dropout=0.25, scale=(1, 2)),
            ),
            dense=HParams(layers=0),
        )
        test(
            (4, 1, 5, 256), conv=HParams(), dense=HParams(layers=1),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),),
            dense=HParams(layers=1),
        )
        test(
            (4, 1, 5, 256),
            conv=HParams(
                conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),
                conv2=HParams(out_channels=128, dropout=0.25, scale=(1, 2)),
                conv4=HParams(out_channels=512, dropout=0.25, scale=(1, 2)),
                conv3=HParams(out_channels=256, dropout=0.25, scale=(1, 2)),
            ),
            dense=HParams(layers=1),
        )

        beta = 2.0
        file_name = os.path.join(self.base_dir, "specvae.pth")
        model = SpecVAE(vae=HParams(beta=beta))
        model.save(file_name)

        model2 = DeepModel.load(file_name)
        with model2.scope() as p:
            assert p.vae.beta == beta


if __name__ == "__main__":
    unittest.main()
