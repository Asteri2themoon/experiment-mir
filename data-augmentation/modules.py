import torch
import torch.nn as nn

from typing import Type, Any


def Activation(function: str):
    activations = {"relu": nn.ReLU(), "sigmoid": nn.Sigmoid(), "tanh": nn.Tanh()}
    assert function in activations

    return activations[function]


class PixelShuffle2D(nn.Module):
    def __init__(self, upsample):
        super(PixelShuffle2D, self).__init__()

        if type(upsample) == int:
            upsample = (upsample, upsample)

        assert (
            type(upsample) == tuple
            and type(upsample[0]) == int
            and type(upsample[1]) == int
        )

        self.__upsample__ = upsample

    def forward(self, x):
        assert len(x.shape) == 4

        bs, c, h, w = x.shape
        output_channel = c // self.__upsample__[0] // self.__upsample__[1]

        reshape = x.view(
            (bs, output_channel, self.__upsample__[0], self.__upsample__[1], h, w),
        )
        transposed = reshape.permute(0, 1, 4, 2, 5, 3)
        return transposed.reshape(
            (bs, output_channel, h * self.__upsample__[0], w * self.__upsample__[1])
        )


class View(nn.Module):
    def __init__(self, shape):
        super(View, self).__init__()

        if type(shape) == torch.Tensor:
            shape = shape.tolist()
        self.__shape__ = tuple(shape)

    def forward(self, x):
        bs = x.shape[0]
        return x.view((bs,) + self.__shape__)


class VariationalBayes(nn.Module):
    def __init__(self, latent_dim: int, input_dim: int, beta: float = 1.0):
        super(VariationalBayes, self).__init__()

        self.beta = beta

        self.fc_mean = nn.Linear(input_dim, latent_dim)
        self.fc_logvar = nn.Linear(input_dim, latent_dim)

        self.fc_z = nn.Linear(latent_dim, input_dim)

        self.mean, self.logvar = None, None

    def reparameterize(self, mean, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mean + eps * std

    @property
    def KL_divergence(self):
        if (self.mean is None) or (self.logvar is None):
            raise Exception(
                "Don't have values for mean or logvar, must do a forward pass before calculating the KL divergence"
            )

        return (
            -0.5
            * self.beta
            * torch.mean(1 + self.logvar - self.mean.pow(2) - self.logvar.exp())
        )

    def encode(self, x, save_state=True):
        if save_state:
            self.mean = self.fc_mean(x)
            self.logvar = self.fc_logvar(x)
            return self.mean
        else:
            return self.fc_mean(x)

    def decode(self, z):
        return self.fc_z(z)

    def forward(self, x, save_state=True):
        if save_state:
            self.mean = self.fc_mean(x)
            self.logvar = self.fc_logvar(x)
            z = self.reparameterize(self.mean, self.logvar)
        else:
            mean = self.fc_mean(x)
            logvar = self.fc_logvar(x)
            z = self.reparameterize(mean, logvar)
        x_tilde = self.fc_z(z)
        return x_tilde
