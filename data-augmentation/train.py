import torch
from torch.utils.tensorboard import SummaryWriter
import pandas as pd

from settings import Settings
from models import DeepModel
import tprint
import tools

import time
import warnings
import copy


class NotImplementedWarning(UserWarning):
    pass


class Training:
    def __init__(
        self,
        dataset_path: str = None,
        from_checkpoint: str = None,
        to_checkpoint: str = None,
        device: str = "cpu",
        hparams: Settings = Settings(),
        logs: str = None,
        exec_train: bool = True,
        exec_validation: bool = True,
        exec_test: bool = True,
        **kwargs,
    ):
        self.hparams = hparams
        self.current_epoch = 0
        self.batch_size = 1

        self.from_checkpoint = from_checkpoint
        self.to_checkpoint = to_checkpoint

        if self.from_checkpoint is not None:
            state = torch.load(self.from_checkpoint)
            self.hparams.from_dict(state["hparams"])
            self.current_epoch = state["current_epoch"] + 1
            self.batch_size = state["batch_size"]

        self.dataset_path = dataset_path
        self.train_loader = None
        self.validation_loader = None
        self.test_loader = None

        self.device = device
        self.epoch = 0

        self.optimizer = None
        self.model = None
        self.loss = None

        if logs is not None:
            self.writer = SummaryWriter(log_dir=logs)
        else:
            self.writer = None

        self.exec_train = exec_train
        self.exec_validation = exec_validation
        self.exec_test = exec_test

    def save_checkpoint(self, file_name: str) -> None:
        torch.save(
            {
                "model": self.model.to_dict(),
                "current_epoch": self.current_epoch,
                "batch_size": self.batch_size,
                "hparams": self.hparams.to_dict(),
                "optimizer": self.optimizer.state_dict(),
            },
            file_name,
        )

    def detach_dict(self, dct: dict) -> None:
        for k, v in dct.items():
            if type(v) == dict:
                self.detach_dict(dct[k])
            elif type(v) == torch.Tensor:
                if (v.shape == []) or (torch.prod(torch.tensor(v.shape)) == 1):
                    dct[k] = v.item()
                else:
                    dct[k] = v.tolist()

    @staticmethod
    def __flatten_dict__(dct: dict, root: str = "") -> dict:
        result = {}
        for k, v in dct.items():
            new_k = root + "/" + k if len(root) != 0 else k
            if type(v) == dict:
                tmp = Settings.__flatten_dict__(v, new_k)
                for k2, v2 in tmp.items():
                    result[k2] = v2
            else:
                result[new_k] = copy.deepcopy(v)
        return result

    def tensorboard_add_scalar(
        self, metrics: dict, root: str = "", step: int = None
    ) -> None:
        if self.writer is not None:
            flatten_metrics = Training.__flatten_dict__(metrics, root=root)
            for k, v in flatten_metrics.items():
                name = k if len(root) == 0 else root + "/" + k
                self.writer.add_scalar(name, v, global_step=step)

    # run training
    def run(self):
        state_optim = None
        state_model = None
        if self.from_checkpoint is not None:
            state = torch.load(self.from_checkpoint)
            state_optim = state["optimizer"]
            state_model = state["model"]
        # load dataset
        self.init(
            self.hparams,
            self.dataset_path,
            state_optim=state_optim,
            state_model=state_model,
            train=self.exec_train,
            validation=self.exec_validation,
            test=self.exec_test,
        )

        self.start = time.time()
        for self.current_epoch in range(self.current_epoch, self.epoch):
            self.model = self.model.to(self.device)
            tprint.print_line(
                f"epoch {self.current_epoch+1}/{self.epoch}", skip_line=True
            )

            # train
            if self.exec_train:
                logs = self.train(self.train_loader, self.current_epoch)

                logs = pd.DataFrame.from_dict(logs)
                logs = self.summarise_metrics(logs)
                self.tensorboard_add_scalar(logs, root="Train", step=self.current_epoch)
                tprint.print_line(
                    f"[train] {self.display_metrics(logs)}", skip_line=True
                )

            # validation
            if self.exec_validation:
                logs = self.validate(self.validation_loader, self.current_epoch)

                logs = pd.DataFrame.from_dict(logs)
                logs = self.summarise_metrics(logs)
                self.tensorboard_add_scalar(
                    logs, root="Validate", step=self.current_epoch
                )
                tprint.print_line(
                    f"[valid] {self.display_metrics(logs)}", skip_line=True
                )

            # backup
            self.save_checkpoint(self.to_checkpoint)

        self.model = self.model.to(self.device)
        # test
        if self.exec_test:
            logs = self.test(self.test_loader)

            logs = pd.DataFrame.from_dict(logs)
            logs = self.summarise_metrics(logs)
            tprint.print_line(f"[test] {self.display_metrics(logs)}", skip_line=True)

            # log hyperparameters with metrics
            if self.writer is not None:
                self.writer.add_hparams(
                    hparam_dict=self.hparams.to_tensorboard_hparams(), metric_dict=logs
                )

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        state_optim: dict = None,
        state_model: dict = None,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.init is not implemented",
            NotImplementedWarning,
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.train_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # validate on one epoch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.validate_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # test on one epoch
    def test_batch(self, data, model, optimizer, loss, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.test_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # calculate metrics
    def summarise_metrics(self, metrics: pd.DataFrame) -> dict:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.summarise_metrics is not implemented",
            NotImplementedWarning,
        )
        return {}

    # display metrics
    def display_metrics(self, metrics: dict) -> str:
        if "loss" in metrics:
            return "loss: {loss:.5f}".format(**metrics)
        else:
            return ""

    # early stop mecanism
    def early_stop(self, metrics: dict):
        return False

    def to_recursive(self, obj, device):
        if type(obj) == list:
            return [self.to_recursive(e, device) for e in obj]
        elif type(obj) == dict:
            return {k: self.to_recursive(v, device) for k, v in obj.items()}
        elif type(obj) == torch.Tensor:
            return obj.to(device)
        return copy.deepcopy(obj)

    def train(self, dataset, epoch: int) -> list:
        self.model.train()

        epoch_start = time.time()
        logs = []
        batch_count = len(dataset)
        for j, batch in enumerate(dataset):
            # train
            batch = self.to_recursive(batch, self.device)
            log = self.train_batch(
                data=batch,
                model=self.model,
                optimizer=self.optimizer,
                loss=self.loss,
                epoch=epoch,
                batch=j,
            )
            self.detach_dict(log)
            logs.append(log)

            # display
            progress = (j + 1) / batch_count
            display_log = "[train] {} {} (remaining time: {} sec)".format(
                tprint.bar(progress, width=40),
                self.display_metrics(log),
                tools.remaining_time(epoch_start, time.time(), progress),
            )
            tprint.print_line(display_log)

        return logs

    def validate(self, dataset, epoch: int) -> list:
        self.model.eval()

        epoch_start = time.time()
        logs = []
        with torch.no_grad():
            batch_count = len(dataset)
            for j, batch in enumerate(dataset):
                # eval
                batch = self.to_recursive(batch, self.device)
                log = self.validate_batch(
                    data=batch,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    epoch=epoch,
                    batch=j,
                )
                self.detach_dict(log)
                logs.append(log)

                # display
                progress = (j + 1) / batch_count
                display_log = "[valid] {} {} (remaining time: {} sec)".format(
                    tprint.bar(progress, width=40),
                    self.display_metrics(log),
                    tools.remaining_time(epoch_start, time.time(), progress),
                )
                tprint.print_line(display_log)
        return logs

    def test(self, dataset) -> list:
        self.model.eval()

        epoch_start = time.time()
        logs = []
        with torch.no_grad():
            batch_count = len(dataset)
            for j, batch in enumerate(dataset):
                # eval
                batch = self.to_recursive(batch, self.device)
                log = self.test_batch(
                    data=batch,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    batch=j,
                )
                self.detach_dict(log)
                logs.append(log)

                # display
                progress = (j + 1) / batch_count
                display_log = "[test] {} {} (remaining time: {} sec)".format(
                    tprint.bar(progress, width=40),
                    self.display_metrics(log),
                    tools.remaining_time(epoch_start, time.time(), progress),
                )
                tprint.print_line(display_log)
        return logs
