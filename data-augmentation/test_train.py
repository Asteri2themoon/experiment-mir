import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torchvision
import pandas as pd

from settings import Settings, HParams
from models import DeepModel
from train import Training
import tools

import unittest
import os
import shutil
import time


class ConvNet(DeepModel):
    __scope__ = "model.conv_net"

    with Settings.default.scope(__scope__) as hparams:
        hparams.conv.filter = 16
        hparams.conv.size = 3
        hparams.linear = 1024

    def __init__(self, **kwargs):
        super(ConvNet, self).__init__(**kwargs)
        with self.scope() as p:
            self.conv = nn.Sequential(
                nn.Conv2d(
                    in_channels=1,
                    out_channels=p.conv.filter,
                    kernel_size=p.conv.size,
                    stride=2,
                    padding=1,
                ),
                nn.Conv2d(
                    in_channels=p.conv.filter,
                    out_channels=p.conv.filter * 2,
                    kernel_size=p.conv.size,
                    stride=2,
                    padding=1,
                ),
                nn.Conv2d(
                    in_channels=p.conv.filter * 2,
                    out_channels=p.conv.filter * 4,
                    kernel_size=p.conv.size,
                    stride=2,
                ),
                nn.Flatten(),
                nn.Linear(p.conv.filter * 4 * 9, p.linear),
                nn.Linear(p.linear, 10),
                nn.LogSoftmax(dim=1),
            )

    def forward(self, x):
        return self.conv(x)


class TestCustomTraining(Training):
    def __init__(self, **kwargs):
        super(TestCustomTraining, self).__init__(**kwargs)

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        state_optim: dict = None,
        state_model: dict = None,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        self.epoch = self.hparams.epoch
        self.batch_size = self.hparams.batch_size

        self.model = ConvNet(**self.hparams.params)

        if state_model is not None:
            self.model.from_dict(state_model)

        self.optimizer = optim.Adam(self.model.parameters(), lr=self.hparams.lr)
        self.loss = nn.NLLLoss()

        if state_optim is not None:
            self.optimizer.load_state_dict(state_optim)

        if train:
            self.train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    dataset_path,
                    train=True,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                batch_size=self.batch_size,
                shuffle=True,
            )

        if validation or test:
            test_validation_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    self.dataset_path,
                    train=False,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                batch_size=self.batch_size,
                shuffle=True,
            )
        if validation:
            self.validation_loader = test_validation_loader
        if test:
            self.test_loader = test_validation_loader

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer.zero_grad()

        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        l.backward()
        optimizer.step()

        return {"loss": l}

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        N = y.shape[0]
        C = y_pred.shape[1]
        success = torch.argmax(y_pred, dim=1).eq(y).sum()
        tp = success
        tn = (C - 1) * success + (C - 2) * (N - success)
        fp = N - success
        fn = N - success

        return {"loss": l, "tp": tp, "tn": tn, "fp": fp, "fn": fn}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        [x, y] = data
        y_pred = model(x)
        l = loss(y_pred, y)

        N = y.shape[0]
        C = y_pred.shape[1]
        success = torch.argmax(y_pred, dim=1).eq(y).sum()
        tp = success
        tn = (C - 1) * success + (C - 2) * (N - success)
        fp = N - success
        fn = N - success

        return {"loss": l, "tp": tp, "tn": tn, "fp": fp, "fn": fn}

    # calculate metrics
    def summarise_metrics(self, metrics: pd.DataFrame) -> dict:
        results = {}

        if "loss" in metrics:
            results["loss"] = metrics["loss"].mean()
        if "tp" in metrics and "tn" in metrics and "fp" in metrics and "fn" in metrics:
            tp = metrics["tp"].sum()
            tn = metrics["tn"].sum()
            fp = metrics["fp"].sum()
            fn = metrics["fn"].sum()

            results["accuracy"] = (tp + tn) / (tp + tn + fp + fn)
            results["precision"] = tp / (tp + fp)
            results["recall"] = tp / (tp + fn)
            results["f1"] = (
                2
                * (results["precision"] * results["recall"])
                / (results["precision"] + results["recall"])
            )

        return results

    # display metrics
    def display_metrics(self, metrics: dict) -> str:
        loss = "loss" in metrics
        acc = "accuracy" in metrics

        results = ""

        if "loss" in metrics:
            results += f"loss: {metrics['loss']:.5f} "

        if "accuracy" in metrics:
            results += f"accuracy: {metrics['accuracy']*100:.2f}%  "
        if "precision" in metrics:
            results += f"precision: {metrics['precision']*100:.2f}% "
        if "recall" in metrics:
            results += f"recall: {metrics['recall']*100:.2f}% "
        if "f1" in metrics:
            results += f"f1: {metrics['f1']*100:.2f}% "

        return results


def tes_without_Training(dataset_path, epoch, batch_size):
    model = nn.Sequential(
        nn.Conv2d(1, 16, 3, stride=2, padding=1),
        nn.Conv2d(16, 32, 3, stride=2, padding=1),
        nn.Conv2d(32, 64, 3, stride=2),
        nn.Flatten(),
        nn.Linear(64 * 9, 128),
        nn.Linear(128, 10),
        nn.LogSoftmax(dim=1),
    )
    optimizer = optim.Adam(model.parameters(), lr=1e-3)
    loss = nn.NLLLoss()

    train_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(
            dataset_path,
            train=True,
            download=True,
            transform=torchvision.transforms.Compose(
                [
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                ]
            ),
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    test_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(
            dataset_path,
            train=False,
            download=True,
            transform=torchvision.transforms.Compose(
                [
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                ]
            ),
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    for i in range(epoch):
        print(f"epoch {i+1}/{epoch}")
        for data in train_loader:
            optimizer.zero_grad()

            [x, y] = data
            y_pred = model(x)
            l = loss(y_pred, y)

            l.backward()
            optimizer.step()

        with torch.no_grad():
            for data in test_loader:
                [x, y] = data
                y_pred = model(x)
                l = loss(y_pred, y)

    tp, tn, fp, fn = 0, 0, 0, 0
    with torch.no_grad():
        for data in test_loader:
            [x, y] = data
            y_pred = model(x)
            l = loss(y_pred, y)

            N = y.shape[0]
            C = y_pred.shape[1]
            success = torch.argmax(y_pred, dim=1).eq(y).sum().item()
            tp += success
            tn += (C - 1) * success + (C - 2) * (N - success)
            fp += N - success
            fn += N - success

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall)

    results = "[test] metrics: "
    results += f"accuracy: {accuracy*100:.2f}%  "
    results += f"precision: {precision*100:.2f}% "
    results += f"recall: {recall*100:.2f}% "
    results += f"f1: {f1*100:.2f}% "
    print(results)


class TestTraining(unittest.TestCase):
    base_dir = "unittest/trainings"
    try:
        os.makedirs(base_dir)
    except OSError:
        pass

    def test_training(self):
        directory = os.path.join(self.base_dir, "tensorboard")
        try:
            shutil.rmtree(directory)
        except OSError as e:
            pass

        start_custom = time.time()

        settings = Settings(
            hparams=HParams(
                lr=1e-4,
                epoch=3,
                batch_size=1024,
                model=HParams(conv_net=HParams(linear=128)),
            )
        )
        test = TestCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            hparams=settings,
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )
        test.run()

        end_custom = time.time()
        duration_custom = end_custom - start_custom
        print(f"Tolat time with Training: {tools.duration_display(duration_custom)}")

        start_classic = time.time()

        tes_without_Training(
            dataset_path=os.path.join(self.base_dir, "mnist"), epoch=3, batch_size=1024
        )

        end_classic = time.time()
        duration_classic = end_classic - start_classic
        print(
            f"Tolat time without Training: {tools.duration_display(duration_classic)}"
        )

        diff = (duration_custom - duration_classic) / duration_classic
        print(f"Training take {diff*100:.1f}% more time")

        state = torch.load(os.path.join(self.base_dir, "checkpoint.pth"))
        state["hparams"]["epoch"] = 5
        torch.save(
            state, os.path.join(self.base_dir, "checkpoint.pth"),
        )

        print(f"Restart Training from the checkpoint (5 epochs in total)")
        test = TestCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            from_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )
        test.run()


if __name__ == "__main__":
    unittest.main()
