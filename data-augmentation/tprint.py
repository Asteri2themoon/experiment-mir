import shutil
import time
import sys


def get_width():
    columns, _ = shutil.get_terminal_size((80, 24))
    return columns


def bar(progress: float, width: int = None, display_percentage: bool = True):
    if width is None:
        width = get_width()
    assert 0.0 <= progress <= 1.0
    assert width >= 3

    width -= 2
    result = (f"[{{:<{width}}}]").format("=" * int(progress * width))

    if display_percentage:
        precentage = "{:.1f}%".format(progress * 100)
        length = len(precentage)
        if length <= width:
            begin = (width + 2 - length) // 2
            end = begin + length
            result = result[:begin] + precentage + result[end:]

    return result


def print_line(str="", columns=None, skip_line=False):
    if columns is None:
        columns = get_width()
    print(
        f"\r{{:<{columns}}}".format(str[: columns - 1]), end="\n" if skip_line else ""
    )
    sys.stdout.flush()


def next_line():
    print()
