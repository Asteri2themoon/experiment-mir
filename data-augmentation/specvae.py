import torch
import torch.nn as nn

from settings import Settings, HParams
from modules import Activation, PixelShuffle2D, View, VariationalBayes
from models import DeepModel, ConvBlock, FcBlock

from typing import Type, Any


class SpecVAE(DeepModel):
    __scope__ = "model.specvae"

    with Settings.default.scope(__scope__) as hparams:
        hparams.input_shape = (5, 256)
        hparams.vae.latent_dim = 64
        hparams.vae.beta = 1.0
        hparams.conv = HParams(
            conv1=HParams(out_channels=64, dropout=0.25, scale=(1, 2)),
            conv2=HParams(out_channels=128, dropout=0.25, scale=(1, 2)),
            conv4=HParams(out_channels=512, dropout=0.25, scale=(1, 2)),
            conv3=HParams(out_channels=256, dropout=0.25, scale=(1, 2)),
        )
        hparams.dense.layers = 2
        hparams.dense.batch_norm = False
        hparams.dense.activation = "relu"
        hparams.dense.dropout = 0.25
        hparams.decoder.type = "deconv"  # deconv or pixel shuffle

    def __init__(self, **kwargs):
        super(SpecVAE, self).__init__(**kwargs)

        with self.scope() as p:
            self.encoder, shape, self.cnn_shape = self.__build_encoder__(p)
            self.vae = VariationalBayes(
                latent_dim=p.vae.latent_dim, input_dim=shape[0], beta=p.vae.beta
            )
            self.decoder = self.__build_decoder__(p, shape, self.cnn_shape[-1])

    @staticmethod
    def __build_encoder__(hparams: HParams):
        assert type(hparams) == HParams or type(hparams) == Settings

        layers = []

        # build encoder
        shape = (1,) + hparams.input_shape
        for _, p in hparams.conv.items():
            # create convolutional block
            conv_block = ConvBlock(
                in_channels=shape[0], out_channels=p.out_channels, kernel_size=(1, 1)
            )
            # update tensor shape
            shape = conv_block.output_shape(shape)
            layers.append(conv_block)
            break
        cnn_shape = [shape]

        # CNN part
        for _, p in hparams.conv.items():
            # create convolutional block
            conv_block = ConvBlock(in_channels=shape[0], **p.params)
            # update tensor shape
            shape = conv_block.output_shape(shape)
            cnn_shape.append(shape)
            # add block
            layers.append(conv_block)

        # 2D to flatten
        layers.append(nn.Flatten())
        shape = (torch.prod(torch.tensor(shape)).item(),)

        # fully-connected part
        for _ in range(hparams.dense.layers):
            # create fully-connected block
            fc_block = FcBlock(
                in_features=shape[0],
                out_features=hparams.vae.latent_dim,
                batch_norm=hparams.dense.batch_norm,
                dropout=hparams.dense.dropout,
                activation=hparams.dense.activation,
            )
            # update tensor shape
            shape = fc_block.output_shape(shape)
            # add block
            layers.append(fc_block)

        return nn.Sequential(*layers), shape, cnn_shape

    @staticmethod
    def __build_decoder__(hparams: HParams, input_shape: Any, cnn_shape: Any):
        assert type(hparams) == HParams or type(hparams) == Settings
        assert type(input_shape) == tuple
        assert type(cnn_shape) == tuple

        layers = []

        # build decoder
        shape = input_shape

        # fully-connected part
        for i in range(hparams.dense.layers):
            # create fully-connected block
            if (i + 1) == hparams.dense.layers:
                out_features = torch.prod(torch.tensor(cnn_shape)).item()
            else:
                out_features = hparams.vae.latent_dim

            fc_block = FcBlock(
                in_features=shape[0],
                out_features=out_features,
                batch_norm=hparams.dense.batch_norm,
                dropout=hparams.dense.dropout,
                activation=hparams.dense.activation,
            )
            # update tensor shape
            shape = fc_block.output_shape(shape)
            # add block
            layers.append(fc_block)

        # 2D to flatten
        layers.append(View(cnn_shape))
        shape = cnn_shape

        # CNN part
        for _, p in hparams.conv.items(reverse=True):
            # create convolutional block
            conv_block = ConvBlock(
                in_channels=shape[0], **p.params, type=hparams.decoder.type
            )
            # update tensor shape
            shape = conv_block.output_shape(shape)
            # add block
            layers.append(conv_block)

        # output conv block
        conv_block = ConvBlock(in_channels=shape[0], out_channels=1, kernel_size=(1, 1))
        layers.append(conv_block)

        return nn.Sequential(*layers)

    @property
    def KL_divergence(self):
        return self.vae.KL_divergence

    def encode(self, x, save_state=True):
        tmp = self.encoder(x)
        z = self.vae.encode(tmp, save_state=save_state)
        return z

    def decode(self, z):
        tmp = self.vae.decode(z)
        x_tilde = self.decoder(tmp)
        return x_tilde

    def forward(self, x, save_state=True):
        tmp = self.encoder(x)
        tmp = self.vae(tmp, save_state=save_state)
        tmp = self.decoder(tmp)
        return tmp
