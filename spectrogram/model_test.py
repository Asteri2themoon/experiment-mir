import torch
import numpy as np

import unittest

import model
import configs
import tools


class TestModels(unittest.TestCase):
    def test_convnet(self):
        hparams = configs.DEFAULT_CONFIGS
        hparams.model_convnet_repete = 3

        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        hparams.model_convnet_repete = 2
        convnet = model.ConvNet(hparams=hparams)
        convnet.eval()

        N = tools.get_n_params(convnet)
        print("ConvNet parameters: {:,}".format(N))

        output = convnet(large_input_tensor[:, :, :5, :])
        output_end = convnet(large_input_tensor[:, :, -5:, :])
        large_output = convnet(large_input_tensor)

        self.assertAlmostEqual(torch.norm(large_output[:, 0, :] - output).item(), 0)
        self.assertAlmostEqual(
            torch.norm(large_output[:, -1, :] - output_end).item(), 0
        )

    def test_convnetflat(self):
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "mel"

        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        hparams.model_convnet_repete = 3
        convnetflat = model.ConvNetFlat(hparams=hparams)
        convnetflat.eval()

        N = tools.get_n_params(convnetflat)
        print("ConvNetFlat parameters: {:,}".format(N))

        output = convnetflat(large_input_tensor[:, :, :5, :])
        output_end = convnetflat(large_input_tensor[:, :, -5:, :])
        large_output = convnetflat(large_input_tensor)

        self.assertAlmostEqual(
            torch.norm(large_output[:, 0, :] - output).item(), 0, places=6
        )
        self.assertAlmostEqual(
            torch.norm(large_output[:, -1, :] - output_end).item(), 0, places=6
        )

    def test_dnn(self):
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "mel"
        hparams.deep_architecture = "dnn"
        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        dnn = model.get_model(hparams)
        dnn.eval()

        N = tools.get_n_params(dnn)
        print("DNN parameters: {:,}".format(N))

        output = dnn(large_input_tensor[:, :, :5, :])
        output_end = dnn(large_input_tensor[:, :, -5:, :])
        large_output = dnn(large_input_tensor)

        self.assertAlmostEqual(
            torch.norm(large_output[:, 0, :] - output).item(), 0, places=6
        )
        self.assertAlmostEqual(
            torch.norm(large_output[:, -1, :] - output_end).item(), 0, places=6
        )

    def test_musiccnn(self):
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "cqt"
        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        musiccnn = model.MusicCNN(hparams=hparams)
        musiccnn.eval()

        N = tools.get_n_params(musiccnn)
        print("MusicCNN parameters: {:,}".format(N))

        output = musiccnn(large_input_tensor[:, :, :5, :])
        output_end = musiccnn(large_input_tensor[:, :, -5:, :])
        large_output = musiccnn(large_input_tensor)

        self.assertTrue(torch.norm(large_output[:, 0, :] - output).item() < 1e-6)
        self.assertTrue(torch.norm(large_output[:, -1, :] - output_end).item() < 1e-6)

    def test_resnetm(self):
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "mel"
        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        resnetm = model.ResNetM(hparams=hparams)
        resnetm.eval()

        N = tools.get_n_params(resnetm)
        print("ResNetM parameters: {:,}".format(N))

        output = resnetm(large_input_tensor[:, :, :5, :])
        output_end = resnetm(large_input_tensor[:, :, -5:, :])
        large_output = resnetm(large_input_tensor)

        self.assertTrue(torch.norm(large_output[:, 0, :] - output).item() < 1e-6)
        self.assertTrue(torch.norm(large_output[:, -1, :] - output_end).item() < 1e-6)

        # ResNetM with bottleneck
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "mel"
        hparams.model_resnetm_bottleneck = True
        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        resnetm = model.ResNetM(hparams=hparams)
        resnetm.eval()

        N = tools.get_n_params(resnetm)
        print("ResNetM parameters: {:,}".format(N))

        output = resnetm(large_input_tensor[:, :, :5, :])
        output_end = resnetm(large_input_tensor[:, :, -5:, :])
        large_output = resnetm(large_input_tensor)

        self.assertTrue(torch.norm(large_output[:, 0, :] - output).item() < 1e-6)
        self.assertTrue(torch.norm(large_output[:, -1, :] - output_end).item() < 1e-6)

    def test_harmonic(self):
        def try_params(bs, in_shape, out_shape, time_bins, freq_bins, filter_size):
            input_tensor = torch.rand((bs, in_shape, time_bins, freq_bins))

            harmonic = model.Harmonic(in_shape, out_shape, kernel_size=filter_size)
            output = harmonic(input_tensor)

            self.assertTrue(output.shape == (bs, out_shape, time_bins, freq_bins))

        try_params(1, 32, 64, 1, 295, (1, 13))
        try_params(1, 32, 64, 5, 295, (1, 13))
        try_params(1, 32, 64, 5, 295, (3, 13))
        try_params(1, 32, 64, 5, 295, (5, 13))
        try_params(4, 32, 64, 1, 295, (1, 13))
        try_params(4, 32, 64, 5, 295, (1, 13))
        try_params(4, 32, 64, 5, 295, (3, 13))
        try_params(4, 32, 64, 5, 295, (5, 13))
        try_params(4, 32, 32, 1, 295, (1, 13))
        try_params(4, 32, 32, 5, 295, (1, 13))
        try_params(4, 32, 32, 5, 295, (3, 13))
        try_params(4, 32, 32, 5, 295, (5, 13))
        try_params(4, 64, 32, 1, 295, (1, 13))
        try_params(4, 64, 32, 5, 295, (1, 13))
        try_params(4, 64, 32, 5, 295, (3, 13))
        try_params(4, 64, 32, 5, 295, (5, 13))

    """
    def test_harmonet(self):
        hparams = configs.DEFAULT_CONFIGS

        hparams.spec_type = "cqt"
        spec_time_bins, spec_freq_bins = hparams.input_shape
        large_input_tensor = torch.rand((1, 1, spec_time_bins * 10, spec_freq_bins))

        harmonet = model.HarmoNet(hparams=hparams)
        harmonet.eval()

        N = tools.get_n_params(harmonet)
        print("HarmoNet parameters: {:,}".format(N))

        output = harmonet(large_input_tensor[:, :, :5, :])
        output_end = harmonet(large_input_tensor[:, :, -5:, :])
        large_output = harmonet(large_input_tensor)

        self.assertTrue(torch.norm(large_output[:, 0, :] - output).item() < 1e-6)
        self.assertTrue(torch.norm(large_output[:, -1, :] - output_end).item() < 1e-6)
    """


if __name__ == "__main__":
    unittest.main()
