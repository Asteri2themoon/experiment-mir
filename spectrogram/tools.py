from magenta.music.protobuf import music_pb2
from magenta.music.midi_synth import synthesize

import sys
import os
from collections import namedtuple
import signal
import time

import configs


class Killer:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit)
        signal.signal(signal.SIGTERM, self.exit)

    def exit(self, signum, frame):
        print("kill signal received, stopping")
        sys.stdout.flush()
        self.kill_now = True


def get_n_params(model):
    pp = 0
    for p in list(model.parameters()):
        nn = 1
        for s in list(p.size()):
            nn = nn * s
        pp += nn
    return pp


def generate_sound(pitchs, sample_rate=configs.DEFAULT_CONFIGS.sample_rate):
    seq = music_pb2.NoteSequence()
    for pitch in pitchs:
        seq.notes.add(pitch=pitch, start_time=1.0, end_time=2.0, velocity=80)
    seq.total_time = 2
    seq.tempos.add(qpm=60)

    return synthesize(seq, sample_rate=sample_rate)


# setup output directory
def create_dir(base, dir):
    try:
        path = os.path.join(base, dir)
        os.makedirs(path)
    except FileExistsError:
        pass
    return path


def timer(start, end):
    hours, rem = divmod(end - start, 3600)
    minutes, seconds = divmod(rem, 60)
    return "{:0>2}:{:0>2}:{:0>6.3f}".format(int(hours), int(minutes), seconds)


def build_args(dic):
    Arguments = namedtuple("Arguments", " ".join(dic.keys()))
    args = Arguments(*dic.values())
    return args


def note_to_str(note):
    notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
    return notes[note % 12] + str((note // 12) - 1)


midi_instrument = [
    "Acoustic Grand Piano",
    "Bright Acoustic Piano",
    "Electric Grand Piano",
    "Honky-tonk Piano",
    "Rhodes Piano",
    "Chorused Electric Piano",
    "Harpsichord",
    "Clavinet",
    "Celesta",
    "Glockenspiel",
    "Music Box",
    "Vibraphone",
    "Marimba",
    "Xylophone",
    "Tubular Bell",
    "Dulcimer/Santur",
    "Drawbar Organ",
    "Percussive B3 Organ",
    "Rock Organ",
    "Church Organ 1",
    "Reed Organ",
    "French Accordion",
    "Harmonica",
    "Bandoneon",
    "Nylon-String Guitar",
    "Steel-String Guitar",
    "Jazz Guitar",
    "Clean Electric Guitar",
    "Muted Electric Guitar",
    "Overdriven Guitar",
    "Distortion Guitar",
    "Guitar Harmonics",
    "Acoustic Bass",
    "Fingered Bass",
    "Picked Bass",
    "Fretless Bass",
    "Slap Bass 1",
    "Slap Bass 2",
    "Synth Bass 1",
    "Synth Bass 2",
    "Violin",
    "Viola",
    "Cello",
    "Contrabass",
    "Tremolo Strings",
    "Pizzicato Strings",
    "Harp",
    "Timpani",
    "String Ensemble",
    "Slow String Ensemble",
    "Synth Strings 1",
    "Synth Strings 2",
    "Choir Aahs",
    "Voice Oohs",
    "Synth Voice",
    "Orchestra Hit",
    "Trumpet",
    "Trombone",
    "Tuba",
    "Muted Trumpet",
    "French Horns",
    "Brass Section 1",
    "Synth Brass 1",
    "Synth Brass 2",
    "Soprano Sax",
    "Alto Sax",
    "Tenor Sax",
    "Baritone Sax",
    "Oboe",
    "English Horn",
    "Bassoon",
    "Clarinet",
    "Piccolo",
    "Flute",
    "Recorder",
    "Pan Flute",
    "Bottle Blow",
    "Shakuhachi",
    "Whistle",
    "Ocarina",
    "Square Lead",
    "Saw Lead",
    "Synth Calliope",
    "Chiffer Lead",
    "Charang",
    "Solo Synth Vox",
    "5th Saw Wave",
    "Bass & Lead",
    "Fantasia Pad",
    "Warm Pad",
    "Polysynth Pad",
    "Space Voice Pad",
    "Bowed Glass Pad",
    "Metal Pad",
    "Halo Pad",
    "Sweep Pad",
    "Ice Rain",
    "Soundtrack",
    "Crystal",
    "Atmosphere",
    "Brightness",
    "Goblin",
    "Echo Drops",
    "Star Theme",
    "Sitar",
    "Banjo",
    "Shamisen",
    "Koto",
    "Kalimba",
    "Bagpipe",
    "Fiddle",
    "Shanai",
    "Tinkle Bell",
    "Agogo",
    "Steel Drums",
    "Woodblock",
    "Taiko",
    "Melodic Tom 1",
    "Synth Drum",
    "Reverse Cymbal",
    "Guitar Fret Noise",
    "Breath Noise",
    "Seashore",
    "Bird",
    "Telephone 1",
    "Helicopter",
    "Applause",
    "Gun Shot",
]
