import torch

import unittest
import math
import os
from collections import namedtuple

import preprocess
import configs
import tools


class TestPreprocess(unittest.TestCase):
    directory = "unittest"

    def test_maestro(self):
        hparams = configs.Parameters(spec_octave_shift=True)
        config_file = os.path.join(self.directory, "config_preprocess.json")
        hparams.to_json(config_file)

        args = {
            "input": os.path.join(self.directory, "maestro-test/maestro-v2.0.0.json"),
            "output": os.path.join(self.directory, "maestro-preprocess-test/"),
            "workers": 2,
            "length": 1,
            "dataset": "maestro",
            "hparams": config_file,
        }

        preprocess.main(tools.build_args(args))

    def test_musicnet(self):
        return
        hparams = configs.Parameters(spec_octave_shift=True)
        config_file = os.path.join(self.directory, "config_preprocess.json")
        hparams.to_json(config_file)

        args = {
            "input": os.path.join(self.directory, ""),
            "output": os.path.join(self.directory, ""),
            "workers": 2,
            "length": 1,
            "dataset": "musicnet",
            "hparams": config_file,
        }

        preprocess.main(tools.build_args(args))


if __name__ == "__main__":
    unittest.main()
