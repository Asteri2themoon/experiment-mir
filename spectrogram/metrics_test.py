import torch

import unittest
import math
import os
from collections import namedtuple

import metrics
import configs
import tools


class TestMetrics(unittest.TestCase):
    directory = "unittest"

    pred = torch.tensor(
        [
            [1, 0, 0, 0, 0],
            [1, 1, 0, 0, 0],
            [1, 1, 0, 0, 0],
            [1, 1, 1, 0, 0],
            [1, 1, 1, 0, 0],
        ],
        dtype=float,
    )

    gt = torch.tensor(
        [
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [1, 1, 1, 0, 0],
            [1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1],
        ],
        dtype=bool,
    )

    def test_metrics(self):
        result = metrics.calculate_metrics(self.pred, self.gt)

        self.assertEqual(result["true_positive"], 8)
        self.assertEqual(result["true_negative"], 9)
        self.assertEqual(result["false_positive"], 3)
        self.assertEqual(result["false_negative"], 5)

        self.assertAlmostEqual(result["precision"], 8 / 11)
        self.assertAlmostEqual(result["recall"], 8 / 13)
        self.assertAlmostEqual(result["accuracy"], 17 / 25)

        self.assertAlmostEqual(result["f1"], 2 / 3)

    def test_metrics_runs(self):

        hparams = configs.Parameters(
            spec_type="mel",
            deep_architecture="conv_net",
            deep_epochs=4,
            deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_convnet.json")
        hparams.to_json(config_file)

        base = {
            "dataset_path": os.path.join(self.directory, "maestro-preprocess/test"),
            "dataset_workers": 2,
            "device": "cpu",
            "hparams": config_file,
            "model_path": os.path.join(
                self.directory, "3da2d843-f8e7-4917-9140-927baac55be5.pth"
            ),
            "output_type": "all",
        }

        metrics.main(
            tools.build_args(
                {
                    **base,
                    "type": "global",
                    "output": os.path.join(self.directory, "metrics"),
                }
            )
        )
        metrics.main(
            tools.build_args(
                {
                    **base,
                    "type": "by_notes",
                    "output": os.path.join(self.directory, "by_notes"),
                }
            )
        )
        metrics.main(
            tools.build_args(
                {
                    **base,
                    "type": "precision_vs_recall",
                    "output": os.path.join(self.directory, "precision_vs_recall"),
                }
            )
        )
        metrics.main(
            tools.build_args(
                {
                    **base,
                    "dataset_path": os.path.join(self.directory, "maestro-preprocess"),
                    "type": "duration",
                    "output": os.path.join(self.directory, "duration"),
                }
            )
        )


if __name__ == "__main__":
    unittest.main()
