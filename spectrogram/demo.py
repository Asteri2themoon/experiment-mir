import torch
import librosa
import magenta.music as mm
import magenta.music.protobuf.music_pb2 as music_pb2
import scipy.io.wavfile as wavfile
import numpy as np
import matplotlib.pyplot as plt

import preprocess
import configs
import model
import metrics

import os
import json


def midi_to_pianoroll(midi_file, hparams=configs.DEFAULT_CONFIGS):
    sequence = mm.midi_file_to_note_sequence(midi_file)
    return preprocess.sequence_to_pianoroll(sequence, hparams=hparams)


def wav2pianoroll(
    model,
    wav,
    threshold=0.5,
    hparams=configs.DEFAULT_CONFIGS,
    device="cpu",
    max_width=1000,
):
    spec = None
    # get spectrogram
    if hparams.spec_type == "mel":
        spec = preprocess.wav_to_mel(wav, hparams=hparams)
    elif hparams.spec_type == "cqt":
        spec = preprocess.wav_to_cqt(wav, hparams=hparams)
    elif hparams.spec_type == "hybrid_cqt":
        spec = preprocess.wav_to_hybrid_cqt(wav, hparams=hparams)

    # numpy array to torch tensor
    spec = torch.t(torch.from_numpy(spec).float())[:, :max_width].to(device)
    freq_bins, time_bins = spec.shape
    spec = spec.view((1, 1, freq_bins, time_bins))

    # inference
    model.eval()
    model = model.to(device)
    pianoroll = model(spec).detach().cpu().numpy()
    del spec

    return np.transpose(pianoroll[0] >= threshold)


def pianoroll2sequence(pianoroll, hparams=configs.DEFAULT_CONFIGS):
    assert (hparams.midi_pitch_max - hparams.midi_pitch_min + 1) == pianoroll.shape[0]

    length = pianoroll.shape[1]
    note_count = pianoroll.shape[0]
    time_step = hparams.spec_hop_length / hparams.sample_rate
    pitch_offset = hparams.midi_pitch_min

    sequence = music_pb2.NoteSequence()

    for note in range(note_count):
        raising, falling = [], []
        prev = 0
        for i in range(length):
            if pianoroll[note, i] != prev:
                if pianoroll[note, i] != 0:
                    raising.append(i)
                if prev != 0:
                    falling.append(i)
            prev = pianoroll[note, i]

        for begin, end in zip(raising, falling):
            sequence.notes.add(
                pitch=pitch_offset + note,
                start_time=begin * time_step,
                end_time=end * time_step,
                velocity=80,
            )

    sequence.total_time = length * time_step

    return sequence


def load_wav(file_name, resample=None):
    rate, wav = wavfile.read(file_name)

    # to mono
    if len(wav.shape) > 1:
        wav = wav.astype(float).mean(axis=1)

    # resample if needed
    if (resample is not None) and (rate != resample):
        wav = librosa.core.resample(wav, rate, resample)

    return wav


def get_dataset_files(json_file, dataset_type="test", file_max=-1):
    with open(json_file, "r") as f:
        maestro_json = json.load(f)
    maestro_base_dir, _ = os.path.split(json_file)

    audio_list = []
    # put task into the queue
    for dic in maestro_json:
        title = dic["canonical_title"]
        midi = os.path.join(maestro_base_dir, dic["midi_filename"])
        audio = os.path.join(maestro_base_dir, dic["audio_filename"])

        if dic["split"] == dataset_type:
            audio_list.append({"title": title, "midi": midi, "audio": audio})
        if file_max > 0 and file_max <= len(audio_list):
            break

    return audio_list


def figures_generate_demo_sample(net, dataset_demo, hparams, device, count=10):
    figures = []
    audio_list = get_dataset_files(dataset_demo, dataset_type="test", file_max=count)
    for audio in audio_list:
        wav = load_wav(audio["audio"], resample=hparams.sample_rate)
        pred = wav2pianoroll(net, wav, hparams=hparams, device=device, max_width=1000)
        midi, instrument = midi_to_pianoroll(audio["midi"])

        N = min(pred.shape[1], midi[0].shape[1], 1000)
        test_metrics = metrics.calculate_metrics(
            torch.from_numpy(pred[:, :N]), torch.from_numpy(midi[0][:, :N])
        )

        plt.gcf()
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 10))
        fig.suptitle(
            "{}\nprecision: {:.1f}% recall: {:.1f}% f1: {:.1f}%".format(
                audio["title"],
                test_metrics["precision"] * 100,
                test_metrics["recall"] * 100,
                test_metrics["f1"] * 100,
            )
        )
        ax1.set_title("ground truth")
        ax1.imshow(midi[0][:, :1000], extent=[0, 1000, 0, 88 * 4], interpolation="none")
        ax1.set_xlabel("time")
        ax1.set_ylabel("notes")
        ax1.axis("off")
        ax2.set_title("prediction")
        ax2.imshow(pred[:, :1000], extent=[0, 1000, 0, 88 * 4], interpolation="none")
        ax2.set_xlabel("time")
        ax2.set_ylabel("notes")
        ax2.axis("off")

        figures.append((os.path.split(audio["audio"])[1], fig))

    return figures


def main(args):
    from magenta.music.midi_synth import synthesize, fluidsynth

    import scipy

    # setup config
    hparams = configs.Parameters()  # default parameters
    if args.hparams is not None:
        hparams.from_json(args.hparams)

    # setup network
    net = model.get_model(hparams)
    net.load(args.model_path)
    net = net.to(args.device)

    figures = []
    audio_list = get_dataset_files(
        args.dataset_demo, dataset_type="test", file_max=args.count
    )

    for audio in audio_list:
        wav = load_wav(audio["audio"], resample=hparams.sample_rate)
        pred = wav2pianoroll(
            net, wav, hparams=hparams, device=args.device, max_width=-1
        )

        seq = pianoroll2sequence(pred, hparams)

        mm.midi_io.note_sequence_to_midi_file(
            seq, output_file=audio["audio"] + "_transcription.midi"
        )

        wav = fluidsynth(seq, hparams.sample_rate, sf2_path=args.soundfont)
        scipy.io.wavfile.write(
            audio["audio"] + "_transcription.wav", hparams.sample_rate, wav
        )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--dataset-workers",
        "-dw",
        type=int,
        default=8,
        help="path to the dataset directory",
    )
    parser.add_argument(
        "--dataset-demo",
        "-dd",
        default=None,
        help="path to a raw maestro dataset to save demo and the end of the training into the tensorflow",
    )
    parser.add_argument(
        "--count", "-c", default=10, type=int, help="demo file generated",
    )
    parser.add_argument(
        "--device", "-d", default="cpu", help="device to run the training"
    )
    parser.add_argument(
        "--hparams",
        "-hp",
        default=None,
        help="hyperparameters configuration, path to the file or json format",
    )
    parser.add_argument("--model-path", "-mp", help="path of the trained model")
    parser.add_argument(
        "--output", "-o", default="runs/outputs", help="output direcotry"
    )
    parser.add_argument("--soundfont", "-s", help="sound font")

    args = parser.parse_args()

    main(args)
