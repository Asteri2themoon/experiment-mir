import torch

import unittest
import math
import os
from collections import namedtuple

import demo
import configs
import tools


class TestDemo(unittest.TestCase):
    directory = "unittest"

    def test_demo(self):
        hparams = configs.Parameters(
            spec_type="mel",
            deep_architecture="conv_net",
            deep_epochs=4,
            deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_convnet.json")
        hparams.to_json(config_file)

        args = {
            "dataset_workers": 2,
            "dataset_demo": os.path.join(
                self.directory, "maestro-demo/maestro-v2.0.0.json"
            ),
            "count": 1,
            "device": "cpu",
            "hparams": config_file,
            "model_path": os.path.join(
                self.directory, "3da2d843-f8e7-4917-9140-927baac55be5.pth"
            ),
            "output": os.path.join(self.directory, "outputs"),
            "soundfont": os.path.join(self.directory, "Nice-Keys-Ultimate-V2.3.sf2"),
        }

        demo.main(tools.build_args(args))


if __name__ == "__main__":
    unittest.main()
