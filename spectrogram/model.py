import torch
import torch.nn as nn
import torch.nn.functional as f

import math

import configs


class DeepModel(nn.Module):
    def __init__(self):
        super(DeepModel, self).__init__()

    def save(self, file_name):
        torch.save({"parameters": self.cpu().state_dict(), "model": self}, file_name)

    def load(self, file_name):
        checkpoint = torch.load(file_name, map_location="cpu")
        self.load_state_dict(checkpoint["parameters"])
        self.eval()

    @staticmethod
    def load_model(file_name):
        checkpoint = torch.load(file_name, map_location="cpu")
        model = checkpoint["model"]
        model.load_state_dict(checkpoint["parameters"])
        model.eval()
        return model


class ScrollingProperty:
    def __init__(
        self, input_width: int = 1, output_width: int = 1, output_offset: int = 0
    ):
        assert input_width >= 1
        assert output_width >= 1
        assert output_offset >= 0

        self.input_width = input_width
        self.output_width = output_width
        self.output_offset = output_offset


class ConvNet(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(ConvNet, self).__init__()

        spec_time_bins, spec_freq_bins = hparams.input_shape

        assert spec_time_bins == 5

        self.output_size = hparams.midi_pitch_max - hparams.midi_pitch_min + 1

        self.sp = ScrollingProperty(input_width=5, output_width=1, output_offset=2)

        repete = hparams.model_convnet_repete

        first_stage = []
        for i in range(repete):
            if i == 0:
                first = nn.Conv2d(1, 48, 3, padding=1)
            else:
                first = nn.Conv2d(48, 48, 3, padding=1)
            first_stage.extend(
                [
                    first,
                    nn.BatchNorm2d(48),
                    nn.ReLU(),
                    nn.Conv2d(48, 48, 3, padding=1),
                    nn.BatchNorm2d(48),
                    nn.ReLU(),
                ]
            )
        second_stage = []
        for i in range(repete):
            if i == 0:
                first = nn.Conv2d(48, 96, 3, padding=1)
            else:
                first = nn.Conv2d(96, 96, 3, padding=1)
            second_stage.extend(
                [first, nn.BatchNorm2d(96), nn.ReLU(),]
            )

        self.conv = nn.Sequential(
            *first_stage,
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(0.25),
            *second_stage,
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(0.25),
        )
        # nn.Flatten(),
        self.fc = nn.Sequential(
            nn.Dropout(0.5),
            nn.Linear(spec_freq_bins // 4 * spec_time_bins * 96, 768),
            nn.ReLU(),
            nn.Linear(768, self.output_size),
            nn.Sigmoid(),
        )

    @property
    def scrolling_property(self):
        return self.sp

    def forward(self, x):
        bs, channels, time_bins, freq_bins = x.shape

        if time_bins == 5:
            conv = self.conv(x)
            output = self.fc(conv.view(bs, -1))
            return output
        elif time_bins > 5:
            outputs = []
            for i in range(time_bins - 4):
                conv = self.conv(x[:, :, i : i + 5, :])
                output = self.fc(conv.reshape(bs, -1))
                outputs.append(output.view((1, 1, -1)))
            outputs = torch.cat(outputs, 1)
            return outputs
        else:
            raise Exception("need at least 5 time bins")


class ConvNetFlat(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(ConvNetFlat, self).__init__()

        spec_time_bins, spec_freq_bins = hparams.input_shape

        self.output_size = hparams.midi_pitch_max - hparams.midi_pitch_min + 1

        repete = hparams.model_convnet_repete

        first_stage = []
        for i in range(repete):
            if i == 0:
                first = nn.Conv2d(1, 48, (1, 3), padding=(0, 1))
            else:
                first = nn.Conv2d(48, 48, (1, 3), padding=(0, 1))
            first_stage.extend(
                [
                    first,
                    nn.BatchNorm2d(48),
                    nn.ReLU(),
                    nn.Conv2d(48, 48, (1, 3), padding=(0, 1)),
                    nn.BatchNorm2d(48),
                    nn.ReLU(),
                ]
            )
        second_stage = []
        for i in range(repete):
            if i == 0:
                first = nn.Conv2d(48, 96, (1, 3), padding=(0, 1))
            else:
                first = nn.Conv2d(96, 96, (1, 3), padding=(0, 1))
            second_stage.extend(
                [first, nn.BatchNorm2d(96), nn.ReLU(),]
            )

        self.net = nn.Sequential(
            *first_stage,
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(0.25),
            *second_stage,
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(0.25),
            nn.Flatten(),
            nn.Linear(spec_freq_bins // 4 * 96, 768),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(768, self.output_size),
            nn.Sigmoid(),
        )

    def forward(self, x):
        bs, channels, time_bins, freq_bins = x.shape

        if time_bins == 5:
            x = x[:, :, 2 : time_bins - 2, :]
            pred = self.net(x)
            return pred.view(bs, self.output_size)
        elif time_bins > 5:
            x = x[:, :, 2 : time_bins - 2, :].view(time_bins - 4, 1, 1, freq_bins)
            pred = self.net(x)
            return pred.view(bs, time_bins - 4, self.output_size)
        else:
            raise Exception("need at least 5 time bins")


class DNN(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(DNN, self).__init__()

        self.input_shape = hparams.input_shape
        self.output_size = hparams.midi_pitch_max - hparams.midi_pitch_min + 1

        self.net = nn.Sequential(
            nn.Dropout(0.1),
            nn.Linear(self.input_shape[1], 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout(0.25),
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout(0.25),
            nn.Linear(512, 512),
            nn.BatchNorm1d(512),
            nn.ReLU(),
            nn.Dropout(0.25),
            nn.Linear(512, self.output_size),
            nn.Sigmoid(),
        )

    def forward(self, x):
        bs, _, time_bins, freq_bins = x.shape
        assert self.input_shape[1] == freq_bins

        if time_bins == 5:
            pred = self.net(x[:, :, 2, :].view(bs, freq_bins))
            return pred
        else:
            pred = self.net(x[:, :, 2:-2, :].view(bs * (time_bins - 4), freq_bins))
            return pred.view(bs, time_bins - 4, self.output_size)


class MusicCNN(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(MusicCNN, self).__init__()

        bins_per_semitons = hparams.cqt_bins_per_octave // 12

        self.output_size = hparams.midi_pitch_max - hparams.midi_pitch_min + 1
        self.input_size = self.output_size * bins_per_semitons

        self.input = nn.Conv2d(
            1,
            hparams.model_musiccnn_filters,
            (1, bins_per_semitons),
            stride=(1, bins_per_semitons),
        )
        if hparams.model_musiccnn_dense:
            self.output = nn.Sequential(
                nn.Conv2d(hparams.model_musiccnn_filters, 1, 1,), nn.Sigmoid()
            )
        else:
            self.output = nn.Sequential(
                nn.Conv2d(hparams.model_musiccnn_filters, 1, 1,), nn.Sigmoid()
            )

        self.by_layer = hparams.model_octavecnn_bylayer
        self.octave = int(math.ceil(self.output_size / 12))

        octave_cnn = [
            nn.Conv2d(
                hparams.model_musiccnn_filters, hparams.model_musiccnn_filters, (1, 13)
            )
            for _ in range(self.octave - 1)
        ]
        self.octave_cnn = nn.ModuleList(octave_cnn)

    def forward(self, x):
        bs, _, time_bins, _ = x.shape

        x = x[:, :, 2 : time_bins - 2, : self.input_size]

        y = self.input(x)

        if self.by_layer:
            pass
        else:
            for i in range(1, self.octave):
                # print("level", i)
                for j, cnn in enumerate(self.octave_cnn[: self.octave - i]):
                    # print("- octave", i + j)
                    y[:, :, :, (i + j) * 12 : (i + j + 1) * 12] = cnn(
                        y[:, :, :, (i + j - 1) * 12 : (i + j + 1) * 12]
                    )

        pred = self.output(y)

        if time_bins == 5:
            return pred[:, :, :, :].view(bs, self.output_size)
        else:
            return pred[:, :, :, : self.output_size].view(
                bs, time_bins - 4, self.output_size
            )


class ResidualConv1D(nn.Module):
    def __init__(
        self,
        in_feature: int,
        out_feature: int,
        filter_size: int = 3,
        bottleneck: bool = False,
        downscale: bool = False,
    ):
        super(ResidualConv1D, self).__init__()

        stride = 2 if downscale else 1
        if downscale:
            self.downscale = nn.Sequential(
                nn.Conv2d(in_feature, out_feature, 1, stride=(1, stride))
            )
        else:
            self.downscale = None

        if bottleneck:
            self.net = nn.Sequential(
                nn.Conv2d(in_feature, out_feature >> 2, 1),
                nn.BatchNorm2d(out_feature >> 2),
                nn.ReLU(),
                nn.Conv2d(
                    out_feature >> 2,
                    out_feature >> 2,
                    (1, filter_size),
                    padding=(0, filter_size // 2),
                    stride=(1, stride),
                ),
                nn.BatchNorm2d(out_feature >> 2),
                nn.ReLU(),
                nn.Conv2d(out_feature >> 2, out_feature, 1),
                nn.BatchNorm2d(out_feature),
                nn.ReLU(),
            )
        else:
            self.net = nn.Sequential(
                nn.Conv2d(
                    in_feature,
                    out_feature,
                    (1, filter_size),
                    padding=(0, filter_size // 2),
                    stride=(1, stride),
                ),
                nn.BatchNorm2d(out_feature),
                nn.ReLU(),
                nn.Conv2d(
                    out_feature,
                    out_feature,
                    (1, filter_size),
                    padding=(0, filter_size // 2),
                ),
                nn.BatchNorm2d(out_feature),
                nn.ReLU(),
            )

    def forward(self, x):
        if self.downscale is not None:
            a = self.net(x)
            b = self.downscale(x)
            return a + b
        else:
            return self.net(x) + x


class ResNetM(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(ResNetM, self).__init__()

        bottleneck = hparams.model_resnetm_bottleneck
        basefilters = hparams.model_resnetm_basefilters
        config_layers = list(hparams.model_resnetm_layers)

        output_size = hparams.input_shape[1] >> len(config_layers)

        layers = []
        layers.append(nn.Conv2d(1, basefilters, 3, padding=1))
        in_feature = basefilters
        for layer in config_layers:
            for j in range(layer):
                out_feature = basefilters << 2 if bottleneck else basefilters
                layers.append(
                    ResidualConv1D(
                        in_feature,
                        out_feature,
                        bottleneck=bottleneck,
                        downscale=(j == 0),
                    )
                )
                in_feature = out_feature
            basefilters <<= 1

        layers.extend(
            [
                nn.AvgPool2d((1, output_size)),
                nn.Flatten(),
                nn.Dropout(hparams.model_resnetm_dropout),
                nn.Linear(in_feature, 88),
                nn.Sigmoid(),
            ]
        )
        self.net = nn.Sequential(*layers)

    def forward(self, x):
        bs, _, time_bins, freq_bins = x.shape

        y = self.net(
            x[:, :, 2 : time_bins - 2, :].view(bs * (time_bins - 4), 1, 1, freq_bins)
        )

        if time_bins == 5:
            return y.view(bs, -1)
        else:
            return y.view(bs, time_bins - 4, -1)


class Harmonic(nn.Module):
    def __init__(self, in_channels: int, out_channels: int = None, kernel_size=13):
        super(Harmonic, self).__init__()

        if out_channels is None:
            out_channels = in_channels

        if type(kernel_size) == int:
            freq_size = kernel_size
            temp_size = 1
        else:
            temp_size, freq_size = kernel_size

        self.cst = nn.Parameter(torch.randn(1, in_channels, 1, freq_size - 1))
        self.cst.requires_grad = True
        
        self.conv = nn.Conv2d(
            in_channels,
            out_channels,
            (temp_size, freq_size),
            padding=(temp_size // 2, 0),
        )

    def forward(self, x):
        bs, _, t, _ = x.shape
        cst = self.cst.repeat((bs, 1, t, 1))
        x = torch.cat((cst, x), 3)
        return self.conv(x)


class HarmoNet(DeepModel):
    def __init__(self, hparams=configs.DEFAULT_CONFIGS):
        super(HarmoNet, self).__init__()

        output_size = hparams.midi_pitch_max - hparams.midi_pitch_min + 1
        bins_per_semitons = hparams.cqt_bins_per_octave // 12
        self.input_size = output_size * bins_per_semitons

        f_setup = hparams.model_harmonet_layers
        blocks = []
        for in_c, out_c in zip(f_setup[:-1], f_setup[1:]):
            if type(in_c) == torch.Tensor:
                in_c = in_c.item()
            if type(out_c) == torch.Tensor:
                out_c = out_c.item()
            blocks.append(
                self._build_block(
                    in_c,
                    out_c,
                    hparams.spec_temp_bins,
                    hparams.model_harmonet_dropout,
                    hparams.model_harmonet_harmonic,
                )
            )
        if type(f_setup[0]) == torch.Tensor:
            begin = f_setup[0].item()
        else:
            begin = f_setup[0]
        if type(f_setup[-1]) == torch.Tensor:
            end = f_setup[-1].item()
        else:
            end = f_setup[-1]
        self.conv = nn.Sequential(
            nn.Conv2d(
                1, begin, (1, bins_per_semitons), stride=(1, bins_per_semitons),
            ),
            *blocks,
            nn.Conv2d(end, 1, 1),
            nn.Sigmoid(),
        )

    def _build_block(
        self,
        in_channels: int,
        out_channels: int,
        time_bins: int,
        dropout: float = 0.25,
        harmonic: bool = True,
    ):
        if harmonic:
            return nn.Sequential(
                Harmonic(in_channels, out_channels, kernel_size=(time_bins, 13)),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(),
                nn.Dropout2d(dropout),
            )
        else:
            return nn.Sequential(
                nn.Conv2d(
                    in_channels,
                    out_channels,
                    kernel_size=(time_bins, 13),
                    padding=(time_bins // 2, 6),
                ),
                nn.BatchNorm2d(out_channels),
                nn.ReLU(),
                nn.Dropout2d(dropout),
            )

    def forward(self, x):
        bs, _, time_bins, _ = x.shape
        y = self.conv(x[:, :, :, :self.input_size])
        if time_bins == 5:
            return y[:,:,2,:].view(bs, -1)
        else:
            return y[:,:,2:-2,:].view(bs, time_bins - 4, -1)


def get_model(hparams=configs.DEFAULT_CONFIGS):
    models = {
        "conv_net": ConvNet,
        "conv_net_flat": ConvNetFlat,
        "music_cnn": MusicCNN,
        "dnn": DNN,
        "resnet_m": ResNetM,
        "harmo_net": HarmoNet,
    }

    assert hparams.deep_architecture in models

    return models[hparams.deep_architecture](hparams)


if __name__ == "__main__":
    import argparse
    import tools

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input", "-i", default="config.json", help="name of the configuration file",
    )

    args = parser.parse_args()

    hparams = configs.Parameters()
    hparams.from_json(args.input)

    net = get_model(hparams=hparams)
    N = tools.get_n_params(net)
    print(
        "model {} containe {:,} learnable parameters".format(
            hparams.deep_architecture, N
        )
    )
