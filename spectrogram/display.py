import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    import argparse
    import tensorflow as tf

    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument("--input", "-i", help="npz file")
    parser.add_argument(
        "--length", "-l", help="sequence length", type=int, default=None
    )

    args = parser.parse_args()

    data = np.load(args.input)

    N = data["vel"].shape[0]
    spectrogram_shape = [0, 1000, 0, 88]
    for i in range(N):
        plt.subplot(3 + N, 1, i + 1)
        plt.axis("off")
        plt.title(data["instruments"][i])
        plt.imshow(
            data["vel"][i, :, : args.length],
            interpolation="none",
            extent=spectrogram_shape,
        )
    plt.subplot(3 + N, 1, N + 1)
    plt.axis("off")
    plt.title("mel spectrogram")
    plt.imshow(
        data["mel"][:, : args.length], interpolation="none", extent=spectrogram_shape
    )
    plt.subplot(3 + N, 1, N + 2)
    plt.axis("off")
    plt.title("CQT")
    plt.imshow(
        data["cqt"][:, : args.length], interpolation="none", extent=spectrogram_shape
    )
    plt.subplot(3 + N, 1, N + 3)
    plt.axis("off")
    plt.title("hybrid CQT")
    plt.imshow(
        data["hybrid_cqt"][:, : args.length],
        interpolation="none",
        extent=spectrogram_shape,
    )
    plt.show()
