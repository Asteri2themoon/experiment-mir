import torch.optim as optim

import configs


def get_optimizer(model, hparams=configs.DEFAULT_CONFIGS):
    if hparams.deep_optimizer == "sgd":
        return optim.SGD(
            [{"params": model.parameters()},],
            lr=hparams.deep_learning_rate,
            momentum=hparams.deep_momentum,
        )
    elif hparams.deep_optimizer == "adam":
        return optim.Adam(
            [{"params": model.parameters()},], lr=hparams.deep_learning_rate,
        )
    else:
        raise Exception("Optimizer {} not found".format(hparams.deep_optimizer))
