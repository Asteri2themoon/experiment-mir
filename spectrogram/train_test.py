import unittest
import shutil
import os
from collections import namedtuple

import configs
import train


class TestTrain(unittest.TestCase):
    directory = "unittest"

    def test_train_convnet(self):
        print("test ConvNet")

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        hparams = configs.Parameters(
            spec_type="mel",
            deep_architecture="conv_net",
            deep_epochs=4,
            deep_batch_size=8,
            model_convnet_repete=3,
        )
        config_file = os.path.join(self.directory, "config_convnet.json")
        hparams.to_json(config_file)

        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers dataset_demo device hparams logs logs_frequency",
        )
        args = Arguments(
            os.path.join(self.directory, "maestro-preprocess"),
            2,
            os.path.join(self.directory, "maestro-demo/maestro-v2.0.0.json"),
            "cpu",
            config_file,
            os.path.join(self.directory, "runs/ConvNet"),
            1,
        )

        train.main(args)

    def test_train_convnetflat(self):
        print("test ConvNetFlat")

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        hparams = configs.Parameters(
            spec_type="mel",
            deep_architecture="conv_net_flat",
            deep_epochs=4,
            deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_convnetflat.json")
        hparams.to_json(config_file)

        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers dataset_demo device hparams logs logs_frequency",
        )
        args = Arguments(
            os.path.join(self.directory, "maestro-preprocess"),
            2,
            os.path.join(self.directory, "maestro-demo/maestro-v2.0.0.json"),
            "cpu",
            config_file,
            os.path.join(self.directory, "runs/ConvNetFlat"),
            1,
        )

        train.main(args)

    def test_train_dnn(self):
        print("test DNN")

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        hparams = configs.Parameters(
            spec_type="mel", deep_architecture="dnn", deep_epochs=4, deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_dnn.json")
        hparams.to_json(config_file)

        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers dataset_demo device hparams logs logs_frequency",
        )
        args = Arguments(
            os.path.join(self.directory, "maestro-preprocess"),
            2,
            os.path.join(self.directory, "maestro-demo/maestro-v2.0.0.json"),
            "cpu",
            config_file,
            os.path.join(self.directory, "runs/DNN"),
            1,
        )

        train.main(args)

    def test_train_musiccnn(self):
        print("test MusicCNN")

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        hparams = configs.Parameters(
            spec_type="cqt",
            deep_architecture="music_cnn",
            deep_epochs=4,
            deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_musiccnn.json")
        hparams.to_json(config_file)

        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers dataset_demo device hparams logs logs_frequency",
        )
        args = Arguments(
            os.path.join(self.directory, "maestro-preprocess"),
            2,
            os.path.join(self.directory, "maestro-demo/maestro-v2.0.0.json"),
            "cpu",
            config_file,
            os.path.join(self.directory, "runs/MusicCNN"),
            1,
        )

        train.main(args)

    def test_train_resnetm(self):
        print("test ResNetM")

        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        hparams = configs.Parameters(
            spec_type="mel",
            deep_architecture="resnet_m",
            deep_epochs=4,
            deep_batch_size=8,
        )
        config_file = os.path.join(self.directory, "config_resnetm.json")
        hparams.to_json(config_file)

        Arguments = namedtuple(
            "Arguments",
            "dataset_path dataset_workers dataset_demo device hparams logs logs_frequency",
        )
        args = Arguments(
            os.path.join(self.directory, "maestro-preprocess"),
            2,
            os.path.join(self.directory, "maestro-demo/maestro-v2.0.0.json"),
            "cpu",
            config_file,
            os.path.join(self.directory, "runs/ResNetM"),
            1,
        )

        train.main(args)


if __name__ == "__main__":
    dir_path_runs = os.path.join(TestTrain.directory, "runs")
    try:
        shutil.rmtree(dir_path_runs)
    except OSError:
        pass

    unittest.main()
