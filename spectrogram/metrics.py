import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

import tprint
import tools

import copy
import math
import os


def true_positive(pred, gt, threshold=0.5, axis=-1):
    assert gt.shape == pred.shape

    pred = pred > threshold
    gt = gt.bool()
    if axis == -1:
        return torch.sum(pred & gt).cpu().numpy()
    else:
        return torch.sum(pred & gt, axis=axis).cpu().numpy()


def true_negative(pred, gt, threshold=0.5, axis=-1):
    assert gt.shape == pred.shape

    pred = pred > threshold
    gt = gt.bool()
    if axis == -1:
        return torch.sum((~pred) & (~gt)).cpu().numpy()
    else:
        return torch.sum((~pred) & (~gt), axis=axis).cpu().numpy()


def false_positive(pred, gt, threshold=0.5, axis=-1):
    assert gt.shape == pred.shape

    pred = pred > threshold
    gt = gt.bool()
    if axis == -1:
        return torch.sum(pred & (~gt)).cpu().numpy()
    else:
        return torch.sum(pred & (~gt), axis=axis).cpu().numpy()


def false_negative(pred, gt, threshold=0.5, axis=-1):
    assert gt.shape == pred.shape

    pred = pred > threshold
    gt = gt.bool()
    if axis == -1:
        return torch.sum(~pred & gt).cpu().numpy()
    else:
        return torch.sum(~pred & gt, axis=axis).cpu().numpy()


def precision(tp, fp):
    try:
        return tp / (tp + fp)
    except ZeroDivisionError:
        return np.nan


def recall(tp, fn):
    try:
        return tp / (tp + fn)
    except ZeroDivisionError:
        return np.nan


def accuracy(tp, tn, fp, fn):
    try:
        return (tp + tn) / (tp + tn + fp + fn)
    except ZeroDivisionError:
        return np.nan


def score_f1(pre, rec):
    try:
        valid = (~np.isnan(pre)) & (~np.isnan(pre))
        return np.where(valid, (2 * pre * rec / (pre + rec)), np.nan)
    except ZeroDivisionError:
        return np.nan


def calculate_metrics(pred, gt, threshold=0.5):
    tp = true_positive(pred, gt, threshold)
    tn = true_negative(pred, gt, threshold)
    fp = false_positive(pred, gt, threshold)
    fn = false_negative(pred, gt, threshold)

    pre = precision(tp, fp)
    rec = recall(tp, fn)
    acc = accuracy(tp, tn, fp, fn)

    f1 = score_f1(pre, rec)

    return {
        "true_positive": tp.tolist(),
        "true_negative": tn.tolist(),
        "false_positive": fp.tolist(),
        "false_negative": fn.tolist(),
        "precision": pre.tolist(),
        "recall": rec.tolist(),
        "accuracy": acc.tolist(),
        "f1": f1.tolist(),
    }


def eval_precision_vs_recall(
    model, dataloader, steps=20, device="cpu", log_frequency=1
):
    # turn model to eval mode
    model = model.to(device)
    model.eval()

    # define steps
    step = 1.0 / steps
    thresholds = torch.linspace(step, 1.0 - step, steps)

    # create tensor to save values
    points = torch.zeros((steps, 2))

    for i, t in enumerate(thresholds):
        # eval metrics
        tp, tn, fp, fn = 0, 0, 0, 0
        batch_count = len(dataloader)
        for j, x in enumerate(dataloader):
            # unpack and upload data on the device
            x, y = x[0]
            x = x.to(device)
            y = y.to(device).ceil()

            # forward
            with torch.no_grad():
                y_p = model(x)

            # evaluate metrics of the model
            y = y.bool()
            tp += true_positive(y_p, y, threshold=t)
            tn += true_negative(y_p, y, threshold=t)
            fp += false_positive(y_p, y, threshold=t)
            fn += false_negative(y_p, y, threshold=t)

            # progress bar
            if (log_frequency != 0) and ((i % log_frequency) == (log_frequency - 1)):
                per = (i * batch_count + j) / (batch_count * steps)
                tprint.print_line(
                    "calculate precision vs recall curve: {}".format(
                        tprint.bar(per, 40)
                    )
                )

        # calculate metrics
        points[i, 0] = precision(tp, fp)
        points[i, 1] = recall(tp, fn)

    # clear screen
    tprint.print_line()

    return points, thresholds


def eval_metrics(
    model, dataloader, loss_func=None, device="cpu", log_frequency=1, axis=-1
):
    tp, tn, fp, fn = 0, 0, 0, 0
    loss = 0

    # turn model to eval mode
    model.to(device)
    model.eval()

    # eval metrics
    batch_count = len(dataloader)
    for i, x in enumerate(dataloader):
        # unpack and upload data on the device
        x, y = x[0]
        x = x.to(device)
        y = y.to(device).ceil()

        # forward
        with torch.no_grad():
            y_p = model(x)

        # evaluate loss of the model
        if loss_func is not None:
            loss += loss_func(y_p, y).item() / batch_count

        # evaluate metrics of the model
        y = y.bool()
        tp += true_positive(y_p, y, axis=axis)
        tn += true_negative(y_p, y, axis=axis)
        fp += false_positive(y_p, y, axis=axis)
        fn += false_negative(y_p, y, axis=axis)

        # progress bar
        if (log_frequency != 0) and ((i % log_frequency) == (log_frequency - 1)):
            tprint.print_line(
                "calculate metrics: {}".format(tprint.bar((i + 1) / batch_count, 40))
            )
    tprint.print_line()

    # calculate metrics
    pre = precision(tp, fp)
    rec = recall(tp, fn)
    acc = accuracy(tp, tn, fp, fn)

    f1 = score_f1(pre, rec)

    # return metrics
    metrics = {
        "true_positive": tp.tolist(),
        "true_negative": tn.tolist(),
        "false_positive": fp.tolist(),
        "false_negative": fn.tolist(),
        "precision": pre.tolist(),
        "recall": rec.tolist(),
        "accuracy": acc.tolist(),
        "f1": f1.tolist(),
    }
    if loss_func is not None:
        metrics["loss"] = loss

    return metrics

def metrics_variation(metrics):
    metrics = copy.deepcopy(metrics)
    for key,values in metrics.items():
        data = np.array(values)
        metrics[key] = (data.mean(), data.std())
    
    return metrics

def eval_notes_duration(dataloader_train, dataloader_validation, dataloader_test):
    def eval_on_dataloader(dataloader):
        durations = 0
        batch_size = None
        batch_count = len(dataloader)
        for i, x in enumerate(dataloader):
            _, y = x[0]
            if batch_size is None:
                batch_size = y.shape[0]
            y = y.numpy()
            notes = np.ceil(y).astype(int)
            notes = np.sum(notes, axis=0)  # sum across batch
            durations = notes + durations

            # progress bar
            tprint.print_line(
                "calculate duration: {}".format(tprint.bar((i + 1) / batch_count, 40))
            )
        tprint.print_line()
        print(batch_count, batch_size, y.shape[0])
        return durations, batch_count*batch_size

    print("calculate duration on train dataset")
    train, N_train = eval_on_dataloader(dataloader_train)
    print("calculate duration on validation dataset")
    validation, N_validation = eval_on_dataloader(dataloader_validation)
    print("calculate duration on test dataset")
    test, N_test = eval_on_dataloader(dataloader_test)
    notes = {"train": train.tolist(),
        "validation": validation.tolist(),
        "test": test.tolist(),
        "P_train":(train/N_train).tolist(),
        "P_validation":(validation/N_validation).tolist(),
        "P_test":(test/N_test).tolist(),
    }
    return notes


def figure_precision_vs_recall(points):
    fig, ax = plt.subplots()
    ax.set_title("Precision vs Recall")
    ax.plot(points[:, 0] * 100, points[:, 1] * 100)
    ax.set_xlabel("recall (percentage)")
    ax.set_ylabel("precision (percentage)")
    return fig


def figure_metrics_notes(metrics, hparams):
    fig, ax = plt.subplots()
    ax.set_title("Metrics by notes")

    def notes(x, pos):
        return tools.note_to_str(int(x))

    formatterx = FuncFormatter(notes)
    ax.xaxis.set_major_formatter(formatterx)

    def percentage(x, pos):
        return "{:.1f}%".format(x * 100)

    formattery = FuncFormatter(percentage)
    ax.yaxis.set_major_formatter(formattery)

    def plot_metric(ax, metrics, key):
        ax.scatter(
            np.arange(hparams.midi_pitch_min, hparams.midi_pitch_max + 1, dtype=int),
            metrics[key],
            label=key,
        )

    plot_metric(ax, metrics, "f1")
    plot_metric(ax, metrics, "precision")
    plot_metric(ax, metrics, "recall")
    ax.legend()
    ax.set_xlabel("notes")
    ax.set_ylabel("Metrics (percentage)")
    return fig


def figure_duration_notes(duration, hparams):
    fig, ax = plt.subplots()
    ax.set_title("Duration by notes")

    def notes(x, pos):
        return tools.note_to_str(int(x))

    formatterx = FuncFormatter(notes)
    ax.xaxis.set_major_formatter(formatterx)

    def plot_metric(ax, metrics, key):
        ax.scatter(
            np.arange(hparams.midi_pitch_min, hparams.midi_pitch_max + 1, dtype=int),
            metrics[key],
            label=key,
        )

    plot_metric(ax, duration, "train")
    plot_metric(ax, duration, "validation")
    plot_metric(ax, duration, "test")
    ax.legend()
    ax.set_xlabel("notes")
    ax.set_ylabel("duration (in timestep)")
    return fig


def figure_metrics(metrics):
    fig, ax = plt.subplots()
    ax.set_title("Metrics")

    def percentage(x, pos):
        return "{:.1f}%".format(x * 100)

    formatter = FuncFormatter(percentage)
    ax.yaxis.set_major_formatter(formatter)

    ax.bar([0, 1, 2], [metrics["precision"], metrics["recall"], metrics["f1"]])
    ax.set_xticks([0, 1, 2])
    ax.set_xticklabels(["Precision", "Recall", "f1"])

    def plot_metric(ax, metrics, key):
        ax.scatter(np.arange(0, len(metrics[key])), metrics[key], label=key)

    ax.set_xlabel("Metrics")
    ax.set_ylabel("Scores (percentage)")
    return fig


def main(args):
    import pandas as pd

    import dataset
    import model
    import configs

    import json

    if args.output != "show":
        plt.switch_backend("agg")

    # setup config
    hparams = configs.Parameters()  # default parameters
    if args.hparams is not None:
        hparams.from_json(args.hparams)

    # setup and load network
    if args.type != "duration":
        net = model.get_model(hparams)
        net.load(args.model_path)
        net = net.to(args.device)
        net.eval()

    # setup dataloader
    if args.type == "duration":
        dataset_train = dataset.get_dataloader(
            os.path.join(args.dataset_path, "train"),
            batch_size=hparams.deep_batch_size,
            spec_type=hparams.spec_type,
            num_workers=args.dataset_workers,
        )
        dataset_validation = dataset.get_dataloader(
            os.path.join(args.dataset_path, "validation"),
            batch_size=hparams.deep_batch_size,
            spec_type=hparams.spec_type,
            num_workers=args.dataset_workers,
        )
        dataset_test = dataset.get_dataloader(
            os.path.join(args.dataset_path, "test"),
            batch_size=hparams.deep_batch_size,
            spec_type=hparams.spec_type,
            num_workers=args.dataset_workers,
        )
    else:
        dataset_test = dataset.get_dataloader(
            args.dataset_path,
            batch_size=hparams.deep_batch_size,
            spec_type=hparams.spec_type,
            num_workers=args.dataset_workers,
        )

    if args.type == "global":
        print("eval on testing set")
        ev_metrics = eval_metrics(net, dataset_test, device=args.device)

        if args.output == "show" or (args.output_type in ["all", "graph"]):
            fig = figure_metrics(ev_metrics)

        if args.output == "show":
            plt.show()
        else:
            if args.output_type in ["all", "graph"]:
                fig.savefig(args.output + ".png")
            if args.output_type in ["all", "json"]:
                with open(args.output + ".json", "w") as f:
                    json.dump(ev_metrics, f, indent=4, sort_keys=True)
            if args.output_type in ["all", "csv"]:
                for key, value in ev_metrics.items():
                    if type(value) != list:
                        ev_metrics[key] = [value]
                df = pd.DataFrame.from_dict(ev_metrics)
                df.to_csv(args.output + ".csv")

    elif args.type == "by_notes":
        print("eval for notes precision")
        ev_metrics = eval_metrics(net, dataset_test, device=args.device, axis=0)

        if args.output == "show" or (args.output_type in ["all", "graph"]):
            fig = figure_metrics_notes(ev_metrics, hparams)

        if args.output == "show":
            plt.show()
        else:
            if args.output_type in ["all", "graph"]:
                fig.savefig(args.output + ".png")
            if args.output_type in ["all", "json"]:
                with open(args.output + ".json", "w") as f:
                    json.dump(ev_metrics, f)
            if args.output_type in ["all", "csv"]:
                df = pd.DataFrame.from_dict(ev_metrics)
                df.to_csv(args.output + ".csv")

    elif args.type == "duration":
        print("eval duration by note")
        durations = eval_notes_duration(dataset_train, dataset_validation, dataset_test)

        if args.output == "show" or (args.output_type in ["all", "graph"]):
            fig = figure_duration_notes(durations, hparams)

        if args.output == "show":
            plt.show()
        else:
            if args.output_type in ["all", "graph"]:
                fig.savefig(args.output + ".png")
            if args.output_type in ["all", "json"]:
                with open(args.output + ".json", "w") as f:
                    json.dump(durations, f)
            if args.output_type in ["all", "csv"]:
                df = pd.DataFrame.from_dict(durations)
                df.to_csv(args.output + ".csv")

    elif args.type == "precision_vs_recall":
        print("eval precision vs recall curve")
        points, threshold = eval_precision_vs_recall(
            net, dataset_test, device=args.device
        )

        if args.output == "show" or (args.output_type in ["all", "graph"]):
            fig = figure_precision_vs_recall(points)

        if args.output == "show":
            plt.show()
        else:
            if args.output_type in ["all", "graph"]:
                fig.savefig(args.output + ".png")
            ev_metrics = {
                "precision": (points[:, 1] * 100).tolist(),
                "recall": (points[:, 0] * 100).tolist(),
                "threshold": threshold.tolist(),
            }
            if args.output_type in ["all", "json"]:
                with open(args.output + ".json", "w") as f:
                    json.dump(ev_metrics, f)
            if args.output_type in ["all", "csv"]:
                df = pd.DataFrame.from_dict(ev_metrics)
                df.to_csv(args.output + ".csv")


if __name__ == "__main__":
    import argparse

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset-path", "-dp", help="path to the dataset directory")
    parser.add_argument(
        "--dataset-workers",
        "-dw",
        type=int,
        default=8,
        help="path to the dataset directory",
    )
    parser.add_argument(
        "--device", "-d", default="cpu", help="device to run the training"
    )
    parser.add_argument(
        "--hparams",
        "-hp",
        default=None,
        help="hyperparameters configuration, path to the file or json format",
    )
    parser.add_argument("--model-path", "-mp", help="path of the trained model")
    parser.add_argument("--output", "-o", help='"show" to display it or file name')
    parser.add_argument(
        "--output-type",
        "-ot",
        default="all",
        choices=["graph", "csv", "json", "all"],
        help="output a graph (png), json or csv",
    )
    parser.add_argument(
        "--type",
        "-t",
        default="global",
        choices=["global", "by_notes", "duration", "precision_vs_recall"],
        help="evaluate global metrics, metrics by notes or precision vs recall curve",
    )

    args = parser.parse_args()

    main(args)
