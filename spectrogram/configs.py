import numpy as np
import torch

import os
import json


class Parameters:
    def __init__(
        self,
        sample_rate: int = 16000,
        spec_hop_length: int = 512,
        spec_type: str = "mel",
        spec_normalize: bool = True,
        spec_temp_bins: int = 5,
        spec_octave_shift: bool = False,
        cqt_n_bins: int = None,
        cqt_bins_per_octave: int = 36,
        cqt_log_amp: bool = True,
        cqt_note_fmin: float = 440.0 * 2 ** (-4),  # from C4 to C0
        mel_n_bins: int = 229,
        mel_htk: bool = True,
        mel_log_amp: bool = True,
        mel_fmin: float = 30.0,
        midi_pitch_min: int = 21,
        midi_pitch_max: int = 108,
        midi_sustain_control: bool = True,
        deep_batch_size: int = 64,
        deep_architecture: str = "conv_net",
        deep_learning_rate: float = 1e-3,
        deep_momentum: float = 0.9,
        deep_epochs: int = 100,
        deep_optimizer: str = "sgd",
        model_musiccnn_filters: int = 64,
        model_musiccnn_depth: int = 4,
        model_musiccnn_filters_freq: int = 3,
        model_musiccnn_batchnorm: bool = True,
        model_musiccnn_residual: bool = False,
        model_musiccnn_dense: bool = False,
        model_convnet_repete: int = 1,
        model_octavecnn_bylayer: bool = False,  # same kernel on one layer if true
        model_resnetm_bottleneck: bool = False,
        model_resnetm_basefilters: int = 32,
        model_resnetm_dropout: float = 0.7,
        model_resnetm_layers: list = [3, 4, 6, 3],
        model_harmonet_layers: list = [64, 64, 128],
        model_harmonet_harmonic: bool = True,
        model_harmonet_dropout: float = 0.25,
    ):
        self.sample_rate = sample_rate

        self.spec_hop_length = spec_hop_length
        self.spec_type = spec_type
        self.spec_normalize = spec_normalize
        self.spec_temp_bins = spec_temp_bins
        self.spec_octave_shift = spec_octave_shift

        self.cqt_bins_per_octave = cqt_bins_per_octave
        self.cqt_log_amp = cqt_log_amp
        self.cqt_note_fmin = cqt_note_fmin
        self.cqt_n_bins = None
        self.cqt_fmin = None

        self.mel_n_bins = mel_n_bins
        self.mel_htk = mel_htk
        self.mel_log_amp = mel_log_amp
        self.mel_fmin = mel_fmin

        self.midi_pitch_min = midi_pitch_min
        self.midi_pitch_max = midi_pitch_max
        self.midi_sustain_control = midi_sustain_control

        self.deep_batch_size = deep_batch_size
        self.deep_architecture = deep_architecture
        self.deep_learning_rate = deep_learning_rate
        self.deep_momentum = deep_momentum
        self.deep_epochs = deep_epochs
        self.deep_optimizer = deep_optimizer

        self.model_musiccnn_filters = model_musiccnn_filters
        self.model_musiccnn_filters_freq = model_musiccnn_filters_freq
        self.model_musiccnn_depth = model_musiccnn_depth
        self.model_musiccnn_batchnorm = model_musiccnn_batchnorm
        self.model_musiccnn_residual = model_musiccnn_residual
        self.model_musiccnn_dense = model_musiccnn_dense
        self.model_convnet_repete = model_convnet_repete
        self.model_octavecnn_bylayer = model_octavecnn_bylayer
        self.model_resnetm_bottleneck = model_resnetm_bottleneck
        self.model_resnetm_basefilters = model_resnetm_basefilters
        self.model_resnetm_dropout = model_resnetm_dropout
        self.model_resnetm_layers = model_resnetm_layers
        self.model_harmonet_layers = model_harmonet_layers
        self.model_harmonet_harmonic = model_harmonet_harmonic
        self.model_harmonet_dropout = model_harmonet_dropout
        self.update()

    def update(self):
        assert self.deep_optimizer in ["sgd", "adam"]
        assert self.spec_type in ["cqt", "hybrid_cqt", "mel"]
        assert self.deep_architecture in [
            "conv_net",
            "conv_net_flat",
            "music_cnn",
            "dnn",
            "resnet_m",
            "harmo_net"
        ]

        # calcul frequency offset to put tone and the center of the bins_per_tone
        bins_per_tone = self.cqt_bins_per_octave / 12
        offset = 2 ** (-(bins_per_tone - 1) / (2 * bins_per_tone) / 12)
        self.cqt_fmin = self.cqt_note_fmin * offset

        if self.cqt_n_bins is None:
            # calcul maximum frequency bins
            fc = self.sample_rate / 2
            c = (self.cqt_bins_per_octave / np.log(2)) * (
                np.log(fc) - np.log(self.cqt_fmin)
            )
            self.cqt_n_bins = int(c)

    def from_json(self, config_file):
        if os.path.isfile(config_file):
            with open(config_file, "r") as f:
                config = json.load(f)

        for key, value in config.items():
            if key in self.__dict__:
                self.__dict__[key] = value
            else:
                raise Exception("parameters {} doesn't existe".format(key))

        self.update()

    def __str__(self):
        return "Paremeters: " + json.dumps(self.__dict__, indent=4, sort_keys=True)

    def to_json(self, file_name):
        with open(file_name, "w") as fp:
            json.dump(self.__dict__, fp, indent=4, sort_keys=True)

    def dict(self):
        return self.__dict__

    def to_hparams(self):
        data = self.__dict__

        for key, value in data.items():
            if type(value) == list:
                data[key] = torch.tensor(value)
        return data

    @property
    def frames_per_second(self):
        return self.sample_rate / self.spec_hop_length

    @property
    def input_shape(self):
        if self.spec_type == "mel":
            return (self.spec_temp_bins, self.mel_n_bins)
        elif self.spec_type == "cqt" or self.spec_type == "hybrid_cqt":
            return (self.spec_temp_bins, self.cqt_n_bins)
        else:
            raise Exception("unkonw type of spectrogram {}".format(self.spec_type))


DEFAULT_CONFIGS = Parameters()

if __name__ == "__main__":
    import argparse

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--output",
        "-o",
        default="config.json",
        help="name of the default generated config file",
    )

    args = parser.parse_args()

    hparams = Parameters()
    hparams.to_json(args.output)
