import torch
import torch.optim as optim
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from dataset import get_dataloader
import model
import configs
import metrics
import tools
import demo
import tprint
import optimizers

import time
import os
import argparse
import uuid


def main(args):
    plt.switch_backend("agg")

    # setup logs
    writer = SummaryWriter(log_dir=args.logs)

    # setup config
    hparams = configs.Parameters()  # default parameters
    if args.hparams is not None:
        hparams.from_json(args.hparams)

    # save hyperparameters
    writer.add_hparams(hparams.to_hparams(), {})

    # setup network
    net = model.get_model(hparams)
    net = net.to(args.device)

    # setup optimizer
    optimizer = optimizers.get_optimizer(net, hparams)

    # setup loss
    weight = None
    if args.weight is not None:
        df = pd.read_csv(args.weight)
        weight = np.array(df["P_train"].sum()/(df["P_train"]*(hparams.midi_pitch_max - hparams.midi_pitch_min)))
        weight = np.clip(weight, 0, 10)
        weight = torch.from_numpy(weight).float().to(args.device)
        print("start with weigths:\n",weight)
    loss = nn.BCELoss(weight = weight)
    
    # setup data loader
    path_train = os.path.join(args.dataset_path, "train")
    path_validation = os.path.join(args.dataset_path, "validation")
    path_test = os.path.join(args.dataset_path, "test")

    dataset_train = get_dataloader(
        path_train,
        batch_size=hparams.deep_batch_size,
        spec_type=hparams.spec_type,
        num_workers=args.dataset_workers,
    )
    dataset_validation = get_dataloader(
        path_validation,
        batch_size=hparams.deep_batch_size,
        spec_type=hparams.spec_type,
        num_workers=args.dataset_workers,
    )
    dataset_test = get_dataloader(
        path_test,
        batch_size=hparams.deep_batch_size,
        spec_type=hparams.spec_type,
        num_workers=args.dataset_workers,
    )

    # monitor time
    start = time.time()

    print("start training")
    # training
    losses = []
    for i in range(hparams.deep_epochs):  # epochs
        training_loss = 0
        validation_loss = 0

        # train
        batch_count = len(dataset_train)
        net.train()
        for j, x in enumerate(dataset_train):  # iterate over training set
            # one step of the training process
            optimizer.zero_grad()

            x, y = x[0]

            x = x.to(args.device)
            y = y.to(args.device).ceil()
            y_p = net(x)
            l = loss(y_p, y)

            l.backward()
            optimizer.step()

            losses.append(l.item())
            training_loss += l.item()

            # logs
            if (args.logs_frequency != 0) and (
                (j % args.logs_frequency) == (args.logs_frequency - 1)
            ):
                log = "[train]epoch {}/{} {} loss: {:.5f} (in {} sec)".format(
                    i + 1,
                    hparams.deep_epochs,
                    tprint.bar((j + 1) / batch_count, width=40),
                    training_loss / (j + 1),
                    tools.timer(start, time.time()),
                )
                tprint.print_line(log)
        training_loss /= batch_count

        log = "[train]epoch {}/{} loss: {:.5f} (in {} sec)".format(
            i + 1, hparams.deep_epochs, training_loss, tools.timer(start, time.time()),
        )
        tprint.print_line(log, skip_line=True)

        # validation
        net.eval()
        ev_metrics = metrics.eval_metrics(
            net,
            dataset_validation,
            loss_func=loss,
            device=args.device,
            log_frequency=args.logs_frequency,
            axis = 0
        )
        # display
        ev_metrics = metrics.metrics_variation(ev_metrics)
        
        def disp_metrics(m, key, pm = u"\u00B1"):
            mean, std = m[key]
            return f"{key}: {mean*100:.2f}{pm}{std*100:.2f}%"

        keys = ['loss', 'accuracy', 'precision', 'recall', 'f1']
        to_display = [disp_metrics(ev_metrics, key) for key in keys]
        tprint.print_line(
            f"[valid]loss:  {' '.join(to_display)}",
            skip_line=True,
        )

        # save loss and metrics on tensorboard
        step = i
        writer.add_scalar("Train/loss", training_loss, step)
        writer.add_scalar("Validation/loss", ev_metrics["loss"], step)
        writer.add_scalar("Validation/accuracy", ev_metrics["accuracy"], step)
        writer.add_scalar("Validation/precision", ev_metrics["precision"], step)
        writer.add_scalar("Validation/recall", ev_metrics["recall"], step)
        writer.add_scalar("Validation/f1", ev_metrics["f1"], step)


    # test
    print("metrics by notes")
    net.eval()
    ev_metrics = metrics.eval_metrics(
        net,
        dataset_test,
        loss_func=loss,
        device=args.device,
        log_frequency=args.logs_frequency,
        axis=0
    )

    fig = metrics.figure_metrics_notes(ev_metrics, hparams)
    writer.add_figure("Test/metrics/metrics_by_notes", fig)

    # test
    print("eval on testing set")
    net.eval()
    ev_metrics = metrics.eval_metrics(
        net,
        dataset_test,
        loss_func=loss,
        device=args.device,
        log_frequency=args.logs_frequency,
    )

    # save model
    model_path = "{}.pth".format(uuid.uuid4())
    net.save(file_name=os.path.join(args.logs, model_path))

    # save test result
    parameters = hparams.to_hparams()
    parameters["model_path"] = model_path
    writer.add_hparams(parameters, ev_metrics)

    # calcul precision vs recall curve
    print("eval precision vs recall curve")
    points, _ = metrics.eval_precision_vs_recall(
        net, dataset_test, device=args.device, log_frequency=args.logs_frequency
    )

    fig = metrics.figure_precision_vs_recall(points)
    writer.add_figure("Test/metrics/precision_vs_recall", fig)

    # generate example for tensorboard
    if args.dataset_demo is not None:
        print("generate example")
        figures = demo.figures_generate_demo_sample(
            net, args.dataset_demo, hparams, args.device
        )
        for name, fig in figures:
            writer.add_figure("Test/demo/{}".format(name), fig)

    print("done in {} sec".format(tools.timer(start, time.time())))
    writer.close()


if __name__ == "__main__":
    import traceback

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument("--dataset-path", "-dp", help="path to the dataset directory")
    parser.add_argument(
        "--dataset-workers",
        "-dw",
        type=int,
        default=8,
        help="path to the dataset directory",
    )
    parser.add_argument(
        "--dataset-demo",
        "-dd",
        default=None,
        help="path to a raw maestro dataset to save demo and the end of the training into the tensorflow",
    )
    parser.add_argument(
        "--device", "-d", default="cpu", help="device to run the training"
    )
    parser.add_argument(
        "--hparams",
        "-hp",
        default=None,
        help="hyperparameters configuration, path to the file or json format",
    )
    parser.add_argument("--logs", "-l", default="runs", help="logs directory")
    parser.add_argument(
        "--logs-frequency", "-lf", type=int, default=10, help="log frequency (in batch)"
    )
    parser.add_argument(
        "--weight", "-w", default=None, help="notes probability file (csv)"
    )

    args = parser.parse_args()

    try:
        main(args)
    except:
        error = traceback.format_exc()
        print(error)
        with open(os.path.join(args.logs, "error.txt"), "w") as f:
            f.write(error)
        writer = SummaryWriter(log_dir=args.logs)
        writer.add_text('Error', error)
