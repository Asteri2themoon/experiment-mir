import librosa
import magenta.music as mm
from magenta.music import sequences_lib
from magenta.music.protobuf import music_pb2
from magenta.music.musicnet_io import note_interval_tree_to_sequence_proto

import configs
import tools
import tprint

import os
import sys
import glob
import json
import math
import copy

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wavfile
import scipy


def wav_to_cqt(wav, hparams=configs.DEFAULT_CONFIGS):
    spec = np.abs(
        librosa.core.cqt(
            wav,
            hparams.sample_rate,
            hop_length=hparams.spec_hop_length,
            fmin=hparams.cqt_fmin,
            n_bins=hparams.cqt_n_bins,
            bins_per_octave=hparams.cqt_bins_per_octave,
        )
    ).astype(np.float32)
    if hparams.cqt_log_amp:
        spec = librosa.power_to_db(spec)
    if hparams.spec_normalize:
        spec = librosa.util.normalize(spec)
    return spec


def wav_to_hybrid_cqt(wav, hparams=configs.DEFAULT_CONFIGS):
    spec = np.abs(
        librosa.core.hybrid_cqt(
            wav,
            hparams.sample_rate,
            hop_length=hparams.spec_hop_length,
            fmin=hparams.cqt_fmin,
            n_bins=hparams.cqt_n_bins,
            bins_per_octave=hparams.cqt_bins_per_octave,
        )
    ).astype(np.float32)
    if hparams.cqt_log_amp:
        spec = librosa.power_to_db(spec)
    if hparams.spec_normalize:
        spec = librosa.util.normalize(spec)
    return spec


def wav_to_mel(wav, hparams=configs.DEFAULT_CONFIGS):
    spec = librosa.feature.melspectrogram(
        wav,
        hparams.sample_rate,
        hop_length=hparams.spec_hop_length,
        fmin=hparams.mel_fmin,
        n_mels=hparams.mel_n_bins,
        htk=hparams.mel_htk,
    ).astype(np.float32)
    if hparams.mel_log_amp:
        spec = librosa.power_to_db(spec)
    if hparams.spec_normalize:
        spec = librosa.util.normalize(spec)
    return spec


def sequence_to_pianoroll(sequence, hparams=configs.DEFAULT_CONFIGS, fill=None):
    velocities = [note.velocity for note in sequence.notes]
    velocity_max = np.max(velocities) if velocities else 0
    velocity_min = np.min(velocities) if velocities else 0
    velocity_range = music_pb2.VelocityRange(min=velocity_min, max=velocity_max)
    if hparams.midi_sustain_control:
        sequence = sequences_lib.apply_sustain_control_changes(sequence)

    seq_inst = {}
    for note in sequence.notes:
        if not (note.program in seq_inst):
            seq_inst[note.program] = []
        seq_inst[note.program].append(note)

    pianorolls = {}
    for key, seq in seq_inst.items():
        tmp = music_pb2.NoteSequence()
        tmp.notes.extend(seq)
        tmp.total_time = sequence.total_time

        tmp = sequences_lib.sequence_to_pianoroll(
            tmp,
            frames_per_second=hparams.frames_per_second,
            min_pitch=hparams.midi_pitch_min,
            max_pitch=hparams.midi_pitch_max,
            min_frame_occupancy_for_label=0.0,
            onset_mode="length_ms",
            onset_length_ms=0.0,
            offset_length_ms=0.0,
            onset_delay_ms=0.0,
            min_velocity=velocity_range.min,
            max_velocity=velocity_range.max,
        )
        pianorolls[tools.midi_instrument[key]] = tmp

    instruments = []
    vels = []
    for key, pianoroll in pianorolls.items():
        vel = np.transpose(pianoroll.active_velocities)

        if fill is not None:
            if fill > vel.shape[1]:
                zeros_shape = (vel.shape[0], fill - vel.shape[1])
                vel = np.append(vel, np.zeros(zeros_shape), axis=1)
            else:
                vel = vel[:,:fill]

        instruments.append(key)
        vels.append(vel.astype(np.float32))

    return vels, instruments


def split_save(job, duration, from_id=0):
    output = job["output"]
    instruments = job["instruments"]
    vel = job["vel"]
    mel = job["mel"]
    cqt = job["cqt"]
    hybrid_cqt = job["hybrid_cqt"]

    assert vel.shape[2] == mel.shape[1] == cqt.shape[1] == hybrid_cqt.shape[1]

    batch = int(math.ceil(mel.shape[1] / duration))

    def fill(x, length, axis):
        if x.shape[axis] != length:
            current_shape = list(x.shape)
            current_shape[axis] = length - current_shape[axis]
            zeros = np.zeros(current_shape)
            x = np.append(x, zeros, axis)
        return x

    current_id = from_id
    for i in range(batch):
        output_file = os.path.join(output, "{}.npz".format(current_id))

        begin, end = i * duration, (i + 1) * duration
        current_vel = fill(vel[:, :, begin:end], duration, 2)
        current_mel = fill(mel[:, begin:end], duration, 1)
        current_cqt = fill(cqt[:, begin:end], duration, 1)
        current_hybrid_cqt = fill(hybrid_cqt[:, begin:end], duration, 1)

        np.savez(
            output_file,
            instruments=instruments,
            vel=current_vel,
            mel=current_mel,
            cqt=current_cqt,
            hybrid_cqt=current_hybrid_cqt,
        )
        current_id += 1

    return current_id


def setup_directory_maestro(output):
    split_dir = {
        "train": tools.create_dir(output, "train"),
        "validation": tools.create_dir(output, "validation"),
        "test": tools.create_dir(output, "test"),
    }
    return split_dir


def feed_queue_maestro(file, split_dir, queue, hparams):
    # read index file
    with open(file, "r") as f:
        maestro_json = json.load(f)
    maestro_base_dir, _ = os.path.split(file)

    # put task into the queue
    for dic in maestro_json:
        midi = os.path.join(maestro_base_dir, dic["midi_filename"])
        audio = os.path.join(maestro_base_dir, dic["audio_filename"])

        split = dic["split"]
        base_output, _ = os.path.splitext(os.path.basename(dic["midi_filename"]))

        queue.put(
            {"midi": midi, "wav": audio, "output": split_dir[split], "hparams": hparams}
        )
    return len(maestro_json)


def preprocess_maestro(job):
    midi, wav, output, hparams = job["midi"], job["wav"], job["output"], job["hparams"]

    sequence = mm.midi_file_to_note_sequence(midi)

    rate, wav = wavfile.read(wav)
    if len(wav.shape) > 1:
        wav = wav.astype(float).mean(axis=1)
    else:
        wav = wav.astype(float)

    def process(wav, rate, sequence, semitons_offset=0):
        seq = copy.deepcopy(sequence)
        offset = 2 ** (-semitons_offset / 12)

        wav = librosa.core.resample(wav, rate, hparams.sample_rate * offset)

        cqt = wav_to_cqt(wav, hparams)
        hybrid_cqt = wav_to_hybrid_cqt(wav, hparams)
        mel = wav_to_mel(wav, hparams)

        for note in seq.notes:
            note.start_time *= offset
            note.end_time *= offset
        seq.total_time *= offset

        vels, instruments = sequence_to_pianoroll(seq, fill=mel.shape[1])
        vels = np.array(vels)

        if semitons_offset > 0:
            vels = np.append(
                np.zeros((vels.shape[0], semitons_offset, vels.shape[2])),
                vels[:, semitons_offset:, :],
                axis=1,
            )
        elif semitons_offset < 0:
            vels = np.append(
                vels[:, :semitons_offset, :],
                np.zeros((vels.shape[0], -semitons_offset, vels.shape[2])),
                axis=1,
            )
        return {
            "output": output,
            "instruments": instruments,
            "vel": np.array(vels),
            "mel": mel.astype(np.float32),
            "cqt": cqt.astype(np.float32),
            "hybrid_cqt": hybrid_cqt.astype(np.float32),
            "hparams": job["hparams"],
        }
    if hparams.spec_octave_shift:
        jobs = [
            process(wav, rate, sequence),
            process(wav, rate, sequence, semitons_offset=-12),
            process(wav, rate, sequence, semitons_offset=12),
        ]
    else:
        jobs = [process(wav, rate, sequence)]

    return jobs


def setup_directory_musicnet(output):
    return tools.create_dir(output, "")


def feed_queue_musicnet(file, output_dir, queue, hparams):
    # read index file
    data = np.load(file, allow_pickle=True, encoding="latin1")

    keys = data.files

    # put task into the queue
    for key in keys:
        queue.put({"data": data, "key": key, "output": output_dir, "hparams": hparams})
    return len(keys)


def preprocess_musicnet(job, sample_rate=44100):
    data, key, output = job["data"], job["key"], job["output"]

    data = np.array(data[key])

    sequence = note_interval_tree_to_sequence_proto(data[1], sample_rate)

    wav = data[0]
    wav = librosa.core.resample(wav, sample_rate, configs.DEFAULT_CONFIGS.sample_rate)

    cqt = wav_to_cqt(wav)
    hybrid_cqt = wav_to_hybrid_cqt(wav)
    mel = wav_to_mel(wav)

    vels, instruments = sequence_to_pianoroll(sequence, fill=mel.shape[1])

    return [
        {
            "output": output,
            "instruments": instruments,
            "vel": np.array(vels),
            "mel": mel.astype(np.float32),
            "cqt": cqt.astype(np.float32),
            "hybrid_cqt": hybrid_cqt.astype(np.float32),
            "hparams": job["hparams"],
        }
    ]


def main(args):
    import time
    import queue
    import threading
    import tensorflow as tf

    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

    killer = tools.Killer()

    dataset_type = ["maestro", "musicnet"]
    if not (args.dataset in dataset_type):
        raise Exception("dataset must be maestro or musicnet")

    print("preprocess {} dataset".format(args.dataset))

    # setup config
    hparams = configs.Parameters()  # default parameters
    if args.hparams is not None:
        hparams.from_json(args.hparams)

    # monitor time
    start = time.time()

    def timer(start, end):
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        return "{:0>2}:{:0>2}:{:02.0f}".format(int(hours), int(minutes), seconds)

    # queue to send task to workers
    q = queue.Queue()
    output = queue.Queue()
    processed = queue.Queue()

    # read index file
    if args.dataset == "maestro":
        split_dir = setup_directory_maestro(args.output)
    elif args.dataset == "musicnet":
        setup_directory_musicnet(args.output)
    else:
        raise Exception('dataset type "{}" not found'.format(args.output))

    # worker to process all the task from the queue
    if args.dataset == "maestro":

        def worker():
            while not killer.kill_now:
                job = q.get()
                if job is None:
                    output.put(None)
                    break
                jobs = preprocess_maestro(job)
                for job in jobs:
                    output.put(job)

    elif args.dataset == "musicnet":

        def worker():
            while not killer.kill_now:
                job = q.get()
                if job is None:
                    output.put(None)
                    break
                jobs = preprocess_musicnet(job)
                for job in jobs:
                    output.put(job)

    else:
        raise Exception('dataset type "{}" not found'.format(args.output))

    def save_output():
        current_id = 0
        finished_worker = 0
        while not killer.kill_now:
            job = output.get()
            if job is None:
                finished_worker += 1
            if finished_worker >= args.workers:
                processed.put(None)
                break
            if job is None:
                continue
            current_id = split_save(job, args.length, from_id=current_id)
            processed.put({"processed": 1, "file_count": current_id})

    # start workers
    threads = []
    for i in range(args.workers):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)
    # start saving thread
    t = threading.Thread(target=save_output)
    t.start()
    threads.append(t)

    # put task into the queue
    if args.dataset == "maestro":
        N = feed_queue_maestro(args.input, split_dir, q, hparams)
    elif args.dataset == "musicnet":
        N = feed_queue_musicnet(args.input, args.output, q, hparams)
    else:
        raise Exception('dataset type "{}" not found'.format(args.output))

    # put stop info
    for i in range(args.workers):
        q.put(None)

    # monitor queue
    processed_file = 0
    while not killer.kill_now:
        tprint.print_line(
            f"{tprint.bar(processed_file / (N*(3 if hparams.spec_octave_shift else 1)),40)} in {timer(start, time.time())}"
        )
        sys.stdout.flush()
        time.sleep(0.1)
        try:
            info = processed.get(block=False)
            if info is None:
                break
            processed_file += info["processed"]
        except queue.Empty:
            pass

    # stop workers
    for t in threads:
        t.join()

    print("\ndone in {}".format(timer(start, time.time())))


if __name__ == "__main__":
    import argparse

    # argments
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--input",
        "-i",
        help="json file of maestro dataset of npz file of the musicnet dataset",
    )
    parser.add_argument("--output", "-o", help="output directory")
    parser.add_argument(
        "--workers", "-w", type=int, default=1, help="number of workers"
    )
    parser.add_argument(
        "--length", "-l", type=int, default=5, help="length of the sequence"
    )
    parser.add_argument(
        "--dataset", "-d", default="maestro", help="dataset type (maestro of musicnet)"
    )
    parser.add_argument(
        "--hparams",
        "-hp",
        default=None,
        help="hyperparameters configuration, path to the file or json format",
    )

    args = parser.parse_args()

    main(args)
